# Additional options for accessing Oracle conditions DB.
from Configurables import CondDB, COOLConfSvc
CondDB(UseOracle = True)
COOLConfSvc(CoralConnectionRetrialTimeOut = 180)
