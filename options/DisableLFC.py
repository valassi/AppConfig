# Additional options to disable the usage of LFC to get the credentials
# to access Oracle servers.
from Gaudi.Configuration import *
from Configurables import COOLConfSvc
def disableLFC():
    COOLConfSvc(UseLFCReplicaSvc = False)
appendPostConfigAction(disableLFC)
