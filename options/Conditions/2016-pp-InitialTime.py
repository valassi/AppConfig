# Set CondDB initial time to end of fill 5451 (last Collision16 fill of 2016)
#
# At the end of the pp running period of 2016, some new HPDs were enabled whose
# QE are not described in CondDB tags prior to cond-20161011.
#
# For the 2016 data-taking software stack, the default initial time was set to the
# end of the year. Hence the initialisation looks for the QEs of the new HPDs
#
# This file sets an initial time compatible with the CondDB tags used for 2016
# HLT and prompt reconstruction, i.e. prior to cond-20161011

from datetime import datetime
from Configurables import EventClockSvc
ecs = EventClockSvc()

dt = datetime(2016, 10, 26, 21, 0) - datetime(1970, 1, 1, 0)
ns = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000000000
ecs.InitialTime = ns
