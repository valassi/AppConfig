### Options to be used in DaVinci to pick up momentum scaling and smearing 
### for Sim06 productions for early 2012

from Configurables import CondDB
CondDB().LocalTags["SIMCOND"] = ["calib-20130325", "calib-20130426", "calib-20130429"]
