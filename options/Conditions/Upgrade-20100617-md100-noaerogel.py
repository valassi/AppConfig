##############################################################################
# File for defining Upgrade conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

# this is needed since sets different DDDB file
LHCbApp().DataType  = "Upgrade"
# Minimal Upgrade Layout with aerogel out of the acceptance
# notice that Boole and Brunel will have to use mul-20100617 tag to process
#    these data
LHCbApp().DDDBtag   = "mul-noaerogel-20100617"
# same as for underlying head-20100504 DDDB tag 
LHCbApp().CondDBtag = "sim-20100510-vc-md100"

##############################################################################
