##############################################################################
# File for defining MC09 conditions with Velo open and magnetic field downward
##############################################################################

from Configurables import LHCbApp

LHCbApp().DDDBtag   = "MC09-20090602"
LHCbApp().CondDBtag = "MC09-20090402-vo-md100"

##############################################################################
