##############################################################################
# File for defining MC09 conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

LHCbApp().DDDBtag   = "MC09-20090602"
LHCbApp().CondDBtag = "MC09-20090402-vc-md100"

##############################################################################
