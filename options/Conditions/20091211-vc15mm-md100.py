##############################################################################
# File for defining conditions with Velo closed at 15 mm and "Down" magnetic
# field with geometry as described on Dec 2009
##############################################################################

from Configurables import LHCbApp

LHCbApp().DDDBtag   = "head-20091120"
LHCbApp().CondDBtag = "sim-20091211-vc15mm-md100"

##############################################################################
