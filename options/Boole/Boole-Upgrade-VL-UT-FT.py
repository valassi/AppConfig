## ############################################################################
## # File for running Boole with Upgrade database
## #
## # Upgrade Detectors: VL UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Boole-Upgrade-VL-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *

from Configurables import Boole
Boole().DetectorDigi = ['VL', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon']
Boole().DetectorLink = ['VL', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Tr']
Boole().DetectorMoni = ['VL', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'MC']
