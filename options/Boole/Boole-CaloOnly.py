## Options to switch off all geometry (and related simulation)
## save that of calorimeters area.
##
## Author: P.Szczypka
## Date:   2012-12-19
##
from Gaudi.Configuration import *

from Configurables import Boole
Boole().DetectorDigi = ['Calo']
Boole().DetectorLink = ['Calo']
Boole().DetectorMoni = ['Calo', 'MC']


