from Configurables import MuonBackground

lowEnergy = MuonBackground("MuonLowEnergy")
lowEnergy.BackgroundType = "LowEnergy"
lowEnergy.SafetyFactors = [0.,0.,0.,0.,0.]

flatSpill = MuonBackground("MuonFlatSpillover")
flatSpill.BackgroundType = "FlatSpillover"
flatSpill.SafetyFactors = [0.,0.,0.,0.,0.]
