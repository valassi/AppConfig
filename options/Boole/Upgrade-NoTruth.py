# $Id: Upgrade-NoTruth.py,v 1.2 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Boole with Upgrade configuration, with Minimal Digi output
#
# Syntax is:
# gaudirun.py Boole/Upgrade-NoTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()
# set special data type for upgrade simulations
theApp.DataType  = "Upgrade"
# enable spillover
theApp.UseSpillover = True

# Switch off MC truth output except for primary vertices information
theApp.DigiType = 'Minimal'

##############################################################################
