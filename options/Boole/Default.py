##############################################################################
# File for running Boole with default settings (full Digi output)
#
# Syntax is:
# gaudirun.py Boole/Default.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()

##############################################################################
