##############################################################################
# File for running Boole with MC09 configuration, with full Digi output
#
# Syntax is:
# gaudirun.py Boole/MC09-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()

##############################################################################
