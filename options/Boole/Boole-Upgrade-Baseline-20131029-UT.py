## ############################################################################
## # File for running Boole with Upgrade database
## #
## # Upgrade Detectors: VP_UVP UT FT_MonoLayer Rich_2019 Calo_NoSpdPrs Muon_NoM1
## #
## # Syntax is:
## #   gaudirun.py Boole-Upgrade-Baseline-20131029-UT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True
CondDB().AllLocalTagsByDataType = ["VP_UVP+RICH_2019+UT", "FT_StereoAngle5", "Muon_NoM1", "Calo_NoSPDPRS"]

from Configurables import Boole
Boole().DataType     = "Upgrade" 

Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon']
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Tr']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'MC']
