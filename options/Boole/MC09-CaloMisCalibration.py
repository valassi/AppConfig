####################################################################################################################
# File for running Boole with MC09 configuration, with Minimal Digi output and a miscalibrated Calorimeter
#
# Syntax is:
# gaudirun.py Boole/MC09-NoTruth.py Conditions/<someTag>.py Boole/MC09-CaloMisCalibration.py <someDataFiles>.py
####################################################################################################################

from Configurables import CondDB
CondDB().LocalTags[ "SIMCOND" ] = [ "calo-20090512-mis", "calo-20090508-mis" ]

from Configurables import CaloDigitAlg
CaloDigitAlg("EcalDigit").GainError=0.0
CaloDigitAlg("HcalDigit").GainError=0.0
CaloDigitAlg("PrsDigit").GainError=0.0
CaloDigitAlg("SpdDigit").GainError=0.0

####################################################################################################################
