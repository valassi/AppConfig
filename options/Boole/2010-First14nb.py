from Gaudi.Configuration import *
def setNBXMuonSpill():
    from Configurables import MuonBackground
    flatSpillover = MuonBackground("MuonFlatSpillover")
    flatSpillover.OutputLevel = 2
    flatSpillover.NBXFullFull = 2

appendPostConfigAction(setNBXMuonSpill)

