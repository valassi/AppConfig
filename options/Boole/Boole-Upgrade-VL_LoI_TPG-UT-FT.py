## ############################################################################
## # File for running Boole with Upgrade database
## #
## # Upgrade Detectors: VL_LoI_TPG UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Boole-Upgrade-VL_LoI_TPG-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True
CondDB().AllLocalTagsByDataType = ["VL_LoI_TPG+UT", "FT"]

from Configurables import Boole
Boole().DataType     = "Upgrade" 

