############################################################################
# File for running Boole with all Baseline Upgrade detectors as of May 2015
# This is same as Boole-Upgrade-Baseline-20150522.py, just adapted for
# Boole v41r0 where possibility to treat Rich1 and Rich2 and Ecal and Hcal
# separately was removed.
############################################################################

from Configurables import Boole, CondDB

CondDB().Upgrade = True
Boole().DataType = "Upgrade"

Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Rich', 'Calo', 'Muon']
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Rich', 'Calo', 'Muon', 'Tr']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Rich', 'Calo', 'Muon', 'Tr', 'MC']
