# Exactly the same as Stripping12 options but only run the
# CharmFull and CharmControl DST streams
from Gaudi.Configuration import *

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# DaVinci Monitoring is done first
#
importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 

from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

from StrippingSettings.Stripping12 import StreamCalibration
from StrippingSettings.Stripping12 import StreamBhadron
from StrippingSettings.Stripping12 import StreamDielectron
from StrippingSettings.Stripping12 import StreamDimuon
from StrippingSettings.Stripping12 import StreamMiniBias
from StrippingSettings.Stripping12 import StreamSemileptonic
from StrippingSettings.Stripping12 import StreamRadiative
from StrippingSettings.Stripping12 import StreamEW
from StrippingSettings.Stripping12 import StreamCharmControl
from StrippingSettings.Stripping12 import StreamCharmMicroDST
from StrippingSettings.Stripping12 import StreamCharmFull
from StrippingSettings.Stripping12 import StreamLeptonicFull
from StrippingSettings.Stripping12 import StreamLeptonicMicroDST

allStreams = [
        StreamCharmControl.stream,
        StreamCharmFull.stream,
        ]

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = allStreams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              stripMicroDSTStreamConf,
                                              stripMicroDSTElements)

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf()
mdstElements = stripMicroDSTElements()

#
# Configuration of Calibration stream 
# to include additional track containers
#
copyExtraTrackData = MoveObjects(objects = [ "Rec/Track/MuonTTTracks",
                                             "Rec/ProtoP/MuonTTProtoP",
					     "Rec/Track/Downstream",
					     "Rec/ProtoP/DownMuonsForTrackEffProtoPMaker",
					     "Rec/Track/VeloMuon",
					     "Rec/ProtoP/VeloMuonForTrackEffLineProtoPMaker" ] )

calibElements = stripDSTElements() + [ copyExtraTrackData ]

#
# Configuration of SelDSTWriter
#
charmStreamName    = StreamCharmMicroDST.stream.name()
leptonicStreamName = StreamLeptonicMicroDST.stream.name()
calibStreamName    = StreamCalibration.stream.name()

SelDSTWriterElements = {
    'default'          : stripDSTElements(),
    charmStreamName    : mdstElements,
    leptonicStreamName : mdstElements,
    calibStreamName    : calibElements
    }


SelDSTWriterConf = {
    'default'            : stripDSTStreamConf(),
    charmStreamName      : mdstStreamConf,
    leptonicStreamName   : mdstStreamConf,
    calibStreamName      : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2010"
DaVinci().EvtMax = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

