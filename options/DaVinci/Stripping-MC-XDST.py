'''Set the Stripping output type to XDST.'''

from Configurables import Stripping

Stripping().DSTType = 'xdst'
