#
#   Stripping selections job (DST output)
#
#   @author A. Poluektov/P. Koppenburg
#   @date 2009-06-23
#

from Gaudi.Configuration import *
from StrippingConf.Configuration import StrippingConf
from Configurables import DaVinci

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
##############################################################################
#
# monitoring
#
importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 
##############################################################################

StrippingConf().ActiveLines = []                      # All lines are active
StrippingConf().ActiveStreams = []                    # All streams are active
StrippingConf().OutputType = "DST"                    # Can be either "ETC" or "DST"
StrippingConf().MainOptions = "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingSelections.py"
DaVinci().EvtMax = -1                                      # Number of events

#############################################################################
#
# Configure DaVinci - to be changed by DIRAC
#

DaVinci().DataType = "2008"                                   # FEST data is 2009
DaVinci().HistogramFile = "DVStripping_Monitor.root"          # Histogram file name here
StrippingConf().StreamFile["BExclusive"] = "Common.dst"       # DST file name here
StrippingConf().StreamFile["Topological"] = "Common.dst"      # SAME DST file name here

# output DST files will be "BExclusive.dst" and "Topological.dst"
# (after the names of active streams present in StrippingSelections)

# to be overwritten by Dirac
EventSelector().Input   = [ "   DATAFILE='Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

