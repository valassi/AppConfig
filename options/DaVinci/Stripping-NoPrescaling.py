'''Disable prescaling in the Stripping.'''

from Configurables import Stripping

Stripping().NoPrescaling = True
