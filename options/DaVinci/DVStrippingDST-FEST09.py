#
#   Stripping selections job (DST output)
#
#   @author J. Palacios/A. Poluektov
#   @date 2009-10-09
#

from Gaudi.Configuration import *
from Configurables import SelDSTWriter, DaVinci
from StrippingConf.Configuration import StrippingConf

sc = StrippingConf()
sc.OutputType = "DST"
sc.__apply_configuration__() # hack needed to ensure activeStreams returns something.
# Dirac should modify OutputFilePrefix.
# SelDSTWriter("StripFEST09DSTWriter").OutputFileSuffix = '012345'
dstWriter = SelDSTWriter("StripFEST09DSTWriter",
                         SelectionSequences = sc.activeStreams(),
                         OutputPrefix = 'Strip'
                         )

DaVinci().DataType = "2008"                   # Default is "MC09"
DaVinci().UserAlgorithms = [ dstWriter.sequence() ]
DaVinci().HltType = ''
DaVinci().L0 = False
