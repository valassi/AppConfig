#
#   Stripping selections job (ETC output)
#
#   @author A. Poluektov
#   @date 2009-06-12
#

from Gaudi.Configuration import *
from StrippingConf.Configuration import StrippingConf
from Configurables import DaVinci

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

StrippingConf().ActiveLines = []                      # All lines are active

StrippingConf().ActiveStreams = []                    # All streams ate active

StrippingConf().OutputType = "ETC"                    # Can be either "ETC" or "DST"

StrippingConf().MainOptions = "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingSelections.py"

#
# Configure DaVinci
#

DaVinci().EvtMax = -1                          # Number of events
DaVinci().DataType = "2008"                    # Default is "DC06"
DaVinci().Simulation   = True                  # It is MC
DaVinci().ETCFile = "DVStripping_ETC.root"
DaVinci().HistogramFile = "DVStripping_Hist.root"
DaVinci().InputType = "RDST"
