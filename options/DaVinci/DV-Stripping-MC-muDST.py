from __future__ import print_function


#from DSTWriters.Configuration import (SelDSTWriter,
#                                      stripMicroDSTStreamConf,
#                                    stripMicroDSTElements)

enablePacking=True

from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, isMC=True)
mdstElements   = stripMicroDSTElements(pack=enablePacking, isMC=True)



SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    'AllStreams' :  mdstElements
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=True),
    'AllStreams' :  mdstStreamConf
    }

#dstWriter = SelDSTWriter( "MyDSTWriter",
 #                         StreamConf = SelDSTWriterConf,
 #                         MicroDSTElements = SelDSTWriterElements,
 #                         OutputFileSuffix ='000000',
 #                         SelectionSequences = sc.activeStreams()
 #                         )

dstWriter = SelDSTWriter( "MyDSTWriter")
print("YEAHH")
print(dstWriter)
dstWriter.StreamConf = SelDSTWriterConf
dstWriter.MicroDSTElements = SelDSTWriterElements
          #                OutputFileSuffix ='000000',
           #               SelectionSequences = sc.activeStreams()
            #              )

