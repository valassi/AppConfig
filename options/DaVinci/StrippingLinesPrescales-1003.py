#######################################################################
#
# PassLinesPrescales. At startup take all
#
# @author P. Koppenburg
# @date 02/03/2010
#
from StrippingSelections.StrippingPass import StrippingPassConf
StrippingPassConf().PassPrescale = 1.0
StrippingPassConf().PhysPrescale = 1.0

