#######################################################################
#
# Prescales for Reco05-Stripping09 first pb^-1
#
# Decided at OPG 12/8/2010
#
# @author P. Koppenburg
# @date 13/08/2010
#
from Configurables import DeterministicPrescaler


# MiniBias stream

DeterministicPrescaler("StrippingMBMicroBiasPreScaler").AcceptFraction = 0.01
DeterministicPrescaler("StrippingMBNoBiasPreScaler").AcceptFraction = 0.01
DeterministicPrescaler("StrippingHlt1L0AnyPreScaler").AcceptFraction = 0.01
DeterministicPrescaler("StrippingMBMicroBiasRateLimitedPreScaler").AcceptFraction = 0.01

# Not Prescaled for CKM:

"""
# Bhadron stream > 5%, pre-scaled x 0.1

DeterministicPrescaler("StrippingB2hhLTUnbiasedLoosePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2pi0LinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2pi0SignalLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhhhLineLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhhhSignalLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhSignalLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2KsLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2KsSignalLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhhLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhhSignalLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingB2DXChi2LooseWithD2hhhhLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingBs2Kst0Kst0LooseLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingBs2PhiKst0bLooseLinePreScaler").AcceptFraction = 0.1

# Charm stream > 5%, pre-scaled x 0.1 

DeterministicPrescaler("StrippingD02KPiforBXXLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDKhPreselLooseLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingLambdacKpipLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDstarPromptWithD02HHNoPtPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDstarVeryLooseWithD02HHLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingD2hhNoPIDLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingStripD2KPP_B_LoosePID_BkgXSPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDKhPreselLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingStripD2KPP_B_LoosePID_BkgPreScaler").AcceptFraction = 0.1

DeterministicPrescaler("StrippingDs2piPhiLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingD2hhNoPIDXsecLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingStripD2KKP_B_LoosePID_BkgXSPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDstar2D0KKmumuRobustPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDst2PiD02KpiBoxPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDst2PiD02pipiBoxPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingStripDstarPromptD02KPiAloneLinePreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingStripD2KKP_B_LoosePID_BkgPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingCcbar2PpbarPreScaler").AcceptFraction = 0.1
"""

# Dimuon stream > 10%, pre-scaled x 0.2 

DeterministicPrescaler("StrippingBd2JpsiKS_KSDD_LinePreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingBd2JpsiKS_KSLL_LinePreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingBs2MuMuNoMuIDLooseLinePreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingDiMuonIncLinePreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingDiMuonIncLooseLinePreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingDiMuonSameSignLinePreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingSimpleBd2KstarMuMuLTPreScaler").AcceptFraction = 0.2

# Dielectron stream > 10%, pre-scaled x 0.2 (0.1)
DeterministicPrescaler("StrippingBiasedIncDiElectronPreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingIncDiElectronPreScaler").AcceptFraction = 0.2
DeterministicPrescaler("StrippingJpsi2eePreScaler").AcceptFraction = 0.2

# EW > 10%, pre-scaled x 0.1
DeterministicPrescaler("StrippingHighPtJetsSelPreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDY2ee1PreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDY2ee2PreScaler").AcceptFraction = 0.1
DeterministicPrescaler("StrippingDY2ee3PreScaler").AcceptFraction = 0.1


# Calibration
DeterministicPrescaler("StrippingJpsiNoPIDPreScaler").AcceptFraction = 0.2

                       
