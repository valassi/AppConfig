from Gaudi.Configuration import *

from CommonParticles.Utils import DefaultTrackingCuts

from Configurables import SelDSTWriter, DaVinci, ProcStatusCheck

from StrippingConf.Configuration import StrippingConf

from StrippingSelections.StreamCalibration import stream as streamCalibration
from StrippingSelections import StreamBhadron
from StrippingSelections import StreamCharm
from StrippingSelections import StreamDielectron
from StrippingSelections import StreamDimuon
from StrippingSelections import StreamMiniBias
from StrippingSelections import StreamSemileptonic
from StrippingSelections import StreamV0
from StrippingSelections import StreamRadiative
from StrippingSelections import StreamEW
from StrippingSelections import StreamDiPhotonDiMuon


streamCalibration.AcceptBadEvents = True

from StrippingConf.StrippingStream import StrippingStream

mergedStream = StrippingStream("Hadronic")
mergedStream.appendLines( StreamBhadron.stream.lines )
mergedStream.appendLines( StreamCharm.stream.lines )


allStreams = [
               streamCalibration, 
               mergedStream,
               StreamDielectron.stream,
               StreamDimuon.stream,
               StreamMiniBias.stream,
               StreamSemileptonic.stream,
               StreamV0.stream,
               StreamRadiative.stream,
               StreamEW.stream,
               StreamDiPhotonDiMuon.stream
             ]


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

filterBadEvents =  ProcStatusCheck()

sc = StrippingConf( Streams = allStreams,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )


dstWriter = SelDSTWriter("MyDSTWriter",
                         SelectionSequences = sc.activeStreams(),
                         OutputFileSuffix = '000000'
                         )


DaVinci().EvtMax = 100                        # Number of events
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

