'''Global configuration for Stripping34r0p1.'''

from Configurables import Stripping

Stripping().Version = 'Stripping34r0p1'
Stripping().TCK = 0x44103401
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
