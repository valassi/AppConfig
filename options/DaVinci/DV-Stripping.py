'''Add the Stripping sequence to DaVinci.'''

from Configurables import DaVinci, Stripping

DaVinci().appendToMainSequence([Stripping().sequence])
DaVinci().ProductionType = 'Stripping'
