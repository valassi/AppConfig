from Gaudi.Configuration import *

from CommonParticles.Utils import DefaultTrackingCuts

from Configurables import SelDSTWriter, DaVinci
from StrippingConf.Configuration import StrippingConf

from StrippingSelections import StreamCalibration
from StrippingSelections import StreamDielectron
from StrippingSelections import StreamDimuon
from StrippingSelections import StreamMiniBias
from StrippingSelections import StreamSemileptonic
from StrippingSelections import StreamV0
from StrippingSelections import StreamRadiative
from StrippingSelections import StreamEW
from StrippingSelections import StreamDiPhotonDiMuon

allStreams = [ 
               StreamCalibration.stream, 
               StreamDielectron.stream,     
               StreamDimuon.stream, 
               StreamMiniBias.stream, 
               StreamSemileptonic.stream, 
               StreamV0.stream,
               StreamRadiative.stream,
               StreamEW.stream,
               StreamDiPhotonDiMuon.stream
             ]
 
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

sc = StrippingConf( Streams = allStreams )

dstWriter = SelDSTWriter("MyDSTWriter",
	SelectionSequences = sc.activeStreams(),
        OutputPrefix = 'Strip',
	OutputFileSuffix = '000000'
        )

DaVinci().EvtMax = 1000                       # Number of events
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

