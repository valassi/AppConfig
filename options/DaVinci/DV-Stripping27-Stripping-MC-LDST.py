"""
Options for building Stripping27. 
"""

#use CommonParticlesArchive
stripping='stripping27'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

## remove GECs for pA
from Configurables import TrackSys
TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)
## add HERSCHEL and VELO raw banks to some lines
for stream in streams:
  for line in stream.lines:
    if line.name() in ['StrippingHeavyIonDiMuonJpsi2MuMuLine', 'StrippingMBMicroBias', 'StrippingMBMicroBiasLowMult', 'StrippingMBNoBias', 'StrippingSingleElectron', 'StrippingHeavyIonOpenCharmD02HHLine', 'StrippingHeavyIonOpenCharmNoPVD02HHBBLine', 'StrippingHeavyIonOpenCharmNoPVD02HHBELine', 'StrippingHeavyIonOpenCharmDst2D0PiLine', 'StrippingHeavyIonOpenCharmDp2KHHLine', 'StrippingHeavyIonOpenCharmDs2KKHLine', 'StrippingHeavyIonOpenCharmLc2PKHLine']:
      if line.RequiredRawEvents:
        line.RequiredRawEvents += ['HC']
        if 'Velo' not in line.RequiredRawEvents:
          line.RequiredRawEvents += ['Velo']
      else:
        line.RequiredRawEvents = ['HC', 'Velo']

AllStreams = StrippingStream("AllStreams")

# These lines are needed to remove duplicates (everything goes to the same stream)
for stream in streams:
    if 'MiniBias' not in stream.name():
        for line in stream.lines:
            dup=False
            for line2 in AllStreams.lines:
                if line2.name()==line.name():
                    dup=True
                    break
            if not dup:
                AllStreams.appendLines([line]) 

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = True

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=True,fileExtension='.ldst')
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42102700)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
