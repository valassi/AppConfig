from Gaudi.Configuration import *

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# DaVinci Monitoring is done first

importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 

from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

#
# MiniBias stream definition

from StrippingConf.StrippingStream import StrippingStream
from StrippingConf.StrippingLine import StrippingLine

MBMicroBiasLine   = StrippingLine( "MBMicroBias"
                                     , HLT = "HLT_PASS_RE('Hlt1MBMicro.*Decision')"
                                     , checkPV = False
                                     , prescale = 0.15
                                     , postscale = 1 )


MBNoBiasLine = StrippingLine( "MBNoBias"
                              , HLT = "HLT_PASS_RE('Hlt1.*NoBias.*Decision')"
                              , checkPV = False
                              , prescale = 0.15
                              , postscale = 1 )

stream = StrippingStream('MiniBias')
stream.appendLines( [ MBMicroBiasLine, MBNoBiasLine ] )

#
# Setup StrippingConf

sc = StrippingConf( Streams = [stream] ) 

#
# Configure DST Writers

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import ( SelDSTWriter,
                                               stripDSTStreamConf,
                                               stripDSTElements )

SelDSTWriterElements = { 'default'  : stripDSTElements()    }

SelDSTWriterConf = { 'default'  : stripDSTStreamConf() }
    

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration

DaVinci().InputType = 'SDST'
DaVinci().DataType = "2010"
DaVinci().EvtMax = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

