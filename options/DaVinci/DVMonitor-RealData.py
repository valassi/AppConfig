#
#   DaVinci monitoring on express stream
#
#   @author P. Koppenburg
#   @date 2010-03-05
#

from Gaudi.Configuration import *
from Configurables import GaudiSequencer

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#
# DaVinci Monitoring is done first
#
importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 
#
# Configure DaVinci
#

from Configurables import DaVinci

DaVinci().IgnoreDQFlags = True
DaVinci().EvtMax = 1000                               # Number of events
DaVinci().HistogramFile = "DVHistos.root"
