'''Global configuration for Stripping32.'''

from Configurables import Stripping

Stripping().Version = 'Stripping32'
Stripping().TCK = 0x42743200
Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
