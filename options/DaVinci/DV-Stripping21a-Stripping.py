"""
Options for building Stripping21a, first restripping of S21 MDST.DST

"""
#set stripping version
stripping="stripping21a"

postfix='a' #needed to redefine streams

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
#from Configurables import RawEventJuggler
#juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "CharmToBeSwum", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]


_lines_list=["StrippingB02DDBeauty2CharmLine","StrippingB02D0PiPiD2HHBeauty2CharmLine","StrippingB02D0D0D02HHD02HHBeauty2CharmLine","StrippingB02DDBeauty2CharmLine", #B2OC
             "StrippingB2HHBDTLine", #BnoC
             "StrippingBs2MuMusNoMuIDLine","StrippingBs2MuMusWideMassLine","StrippingBs2MuMusBd2JPsiKstLine","StrippingBs2MuMusBs2JPsiPhiLine","StrippingBs2MuMusBs2KKLTUBLine",
             "StrippingBs2MuMusBu2JPsiKLine","StrippingBs2MuMusLTUBLine","StrippingBs2MuMusNoMuIDLine","StrippingBs2MuMusSSLine", #RD,BsMuMu
             "StrippingBLVLinesB2DpiLine","StrippingBLVLinesB2LcmuLine","StrippingBLVLinesB2LcpLine","StrippingBLVLinesBs2DspiLine",
             "StrippingBLVLinesLa2KmuLine","StrippingBLVLinesLb2DmuLine","StrippingBLVLinesLb2DsmuLine","StrippingBLVLinesLb2KmuLine","StrippingBLVLinesLb2LcpiLine", #RD,BLV
             "StrippingB24pLinesB24pLine","StrippingB24pLinesB2JpsiKpiLine", #RD, StrippingB24pLinesB24pLine
             "StrippingB23MuLinesB23MuLine","StrippingB23MuLinesB23PiLine","StrippingB23MuLinesB2MueeLine", #RD,B23Mu
             "StrippingD23MuLinesD23MuLine","StrippingD23MuLinesD23PiLine","StrippingD23MuLinesD2MueeLine", #RD,D23Mu
             "StrippingLc23MuLinesLc23muLine","StrippingLc23MuLinesLc2mueeLine","StrippingLc23MuLinesLc2pKpiLine","StrippingLc23MuLinesLc2peeLine","StrippingLc23MuLinesLc2pmumuLine", #RD,Lc23Mu
             "StrippingB2KstTauTau_TauMu_Line","StrippingB2KstTauTau_TauMu_SameSign_Line","StrippingB2KstTauTau_TauTau_Line","StrippingB2KstTauTau_TauTau_SameSign_Line" #RD,KstTauTau
             ]

streamstorun = []

for stream in streams:
    
    if stream.name() in mdstStreams:
        stream.lines = [line for line in stream.lines if (line.name() in _lines_list)]
    else:
        stream.lines = []
    for line in stream.lines:
        if line.name() in _lines_list:
            if line._prescale!=1.0:
                line._prescale=1.0
            if line._postscale!=1.0:
                line._postscale=1.0
    if len(stream.lines)>0 and stream.name() in mdstStreams:
        streamstorun.append(stream.clone(stream.name()+postfix))


stripTESPrefix = 'StripMDSTDST'

from Configurables import ProcStatusCheck, GaudiSequencer
from PhysConf.Filters import LoKi_Filters
flts = LoKi_Filters(VOID_Code = "( TrSource(TrSOURCE('/Event/Rec/Track/Best', TrLONG))"\
                                " >> ( sum( TrPT,TrP < 1 * TeV ) > 1 * TeV ) )" ,
                    VOID_Preambulo = ["from LoKiTracks.decorators import *" ,
                                      "from LoKiCore.functions    import * ",
                                      "from GaudiKernel.SystemOfUnits import *"])
filterBadEvents = GaudiSequencer("BadEventFilter",
                                  ModeOR = True,
                                  Members = [ flts.sequencer("GECFilter"),
                                              ProcStatusCheck() ] )
streamFilter = { 'default'  : filterBadEvents,
                 'MiniBias'+postfix : ProcStatusCheck() }

sc = StrippingConf( Streams = streamstorun,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = streamFilter,
                    TESPrefix = stripTESPrefix,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=False)
mdstElements   = stripMicroDSTElements(pack=enablePacking)
#fix to propagate the raw banks also with selectiveRawEvent=False
mdstStreamConf.extraItems+=["/Event/Velo/RawEvent#1","/Event/Tracker/RawEvent#1","/Event/Rich/RawEvent#1","/Event/Calo/RawEvent#1","/Event/Muon/RawEvent#1"]

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname+postfix       : stripMicroDSTElements(pack=enablePacking,rootintesforpackcluster="/Event/"+charmMicroDSTname+"/"),
    leptonicMicroDSTname+postfix    : stripMicroDSTElements(pack=enablePacking,rootintesforpackcluster="/Event/"+leptonicMicroDSTname+"/"),
    pidMicroDSTname+postfix         : stripMicroDSTElements(pack=enablePacking,rootintesforpackcluster="/Event/"+pidMicroDSTname+"/"),
    bhadronMicroDSTname+postfix     : stripMicroDSTElements(pack=enablePacking,rootintesforpackcluster="/Event/"+bhadronMicroDSTname+"/"),
    #'RestripMDST'                  : mdstElements
    }

SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False),
    charmMicroDSTname+postfix        : mdstStreamConf,
    leptonicMicroDSTname+postfix     : mdstStreamConf,
    bhadronMicroDSTname+postfix      : mdstStreamConf,
    pidMicroDSTname+postfix          : stripCalibMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=False)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )

#
# Add stripping TCK
#
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/StripMDSTDST/Phys/DecReports', TCK=0x3613210A)

#
#Configure DaVinci
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
