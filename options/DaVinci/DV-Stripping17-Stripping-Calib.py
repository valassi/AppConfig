"""
Options for building Stripping17 with
Calibration stream - includes tracking lines
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream 
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)


streams = []
streams.append( quickBuild('Calibration') )
streams.append( quickBuild('PID') )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              stripMicroDSTStreamConf,
                                              stripMicroDSTElements,
                                              stripCalibMicroDSTStreamConf
                                              )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf()
mdstElements   = stripMicroDSTElements()


#
# Add traks + protoparticles for TrackEff lines
#
copyExtraTrackData = MoveObjects(objects = [ "Rec/TrackEffMuonTT_SelMuonTTPParts/ProtoParticles",
                                             "Rec/TrackEffMuonTT_SelMakeMuonTT/Tracks",
					     "Rec/Downstream/Tracks",
					     "Rec/Downstream/FittedTracks",
					     "Rec/ProtoP/DownMuonTrackEffDownMuonNominalProtoPMaker/ProtoParticles",
					     "Rec/VeloMuon/Tracks",
					     "Rec/ProtoP/VeloMuonTrackEffVeloMuonProtoPMaker/ProtoParticles" ])
				 

calibElements = stripDSTElements() + [ copyExtraTrackData ]

#
# Configuration of SelDSTWriter
#

SelDSTWriterElements = {
    'Calibration' : calibElements,
    'PID'         : mdstElements 
    }


SelDSTWriterConf = {
    'Calibration' : stripDSTStreamConf(),
    'PID'         : stripCalibMicroDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2011"
DaVinci().EvtMax = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().UseTrigRawEvent=True
