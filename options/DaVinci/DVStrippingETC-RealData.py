#
#   Stripping selections ETC writing on real data
#   Used in full stream reconstruction
#
#   @author P. Koppenburg
#   @date 2010-03-01
#

from Gaudi.Configuration import *
from Configurables import GaudiSequencer

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#
# DaVinci Monitoring is done first
#
importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 
#
# Stripping job configuration
#
from StrippingConf.Configuration import StrippingConf
from StrippingSelections.Streams import allStreams

sc = StrippingConf( Streams = allStreams )
sc.OutputType = "ETC"                    # Can be either "ETC" or "DST"

from Configurables import EventTuple, TupleToolSelResults

tag = EventTuple("TagCreator")
tag.EvtColsProduce = True
tag.ToolList = [ "TupleToolEventInfo", "TupleToolRecoStats", "TupleToolSelResults"  ]
tag.addTool(TupleToolSelResults)

tag.TupleToolSelResults.Selections = sc.selections()  # Add the list of stripping selections to TagCreator

#
# Configure DaVinci
#

from Configurables import DaVinci

DaVinci().appendToMainSequence( [ sc.sequence() ] )   # Append the stripping selection sequence to DaVinci
DaVinci().MoniSequence += [ tag ]                     # Append the TagCreator to DaVinci
DaVinci().EvtMax = 1000                               # Number of events
DaVinci().DataType = "2010"     
DaVinci().Simulation = False      
DaVinci().ETCFile = "etc.root"
DaVinci().HistogramFile = "DVHistos.root"
