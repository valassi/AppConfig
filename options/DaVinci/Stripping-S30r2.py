'''Global configuration for Stripping30r2.'''

from Configurables import Stripping, TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

Stripping().Version = 'Stripping30r2'
Stripping().TCK = 0x41503020
Stripping().MaxCandidates = 2000
