"""
Options for building Stripping17b with strict ordering
of streams such that the micro-DSTs come last.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17b'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

streams.append( quickBuild('Dimuon') )
streams.append( quickBuild('Semileptonic') )
streams.append( quickBuild('Bhadron') )
streams.append( quickBuild('Radiative') )
streams.append( quickBuild('CharmCompleteEvent') )
streams.append( quickBuild('EW') )

_charm_micro    = quickBuild('Charm')
_pid            = quickBuild('PID') 

streams.append( _charm_micro )
streams.append( _pid )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = 'Strip'    
                    )

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf()
mdstElements   = stripMicroDSTElements()

charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID' 

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(),
    charmMicroDSTname      : mdstElements,
    pidMicroDSTname        : mdstElements
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(),
    charmMicroDSTname      : mdstStreamConf,
    pidMicroDSTname        : stripCalibMicroDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2011"
DaVinci().EvtMax = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

