#Options file to run the EventAccounting algorithm

# The LumiSeq is cleared for simulation productions,
#so this must be added back in for FSRs during the Davinc step

from Configurables import DaVinci, EventAccounting
DaVinci().EventPreFilters.insert( 0, EventAccounting() ) 
