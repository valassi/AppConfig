# Patch for decoding HltDecReports when reading rDST with DV v25r2*
from Configurables import HltDecReportsDecoder
HltDecReportsDecoder(). InputRawEventLocation = "pRec/RawEvent"
