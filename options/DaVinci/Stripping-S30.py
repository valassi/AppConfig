'''Global configuration for Stripping30.'''

from Configurables import Stripping, TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

Stripping().Version = 'Stripping30'
Stripping().TCK = 0x41323000
Stripping().MaxCandidates = 2000
