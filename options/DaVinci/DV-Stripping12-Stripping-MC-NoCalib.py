from Gaudi.Configuration import *

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# DaVinci Monitoring is done first
#
importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 

from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingStream import StrippingStream

#
# No microDST lines are included
#
#from StrippingSettings.Stripping12 import StreamCalibration
from StrippingSettings.Stripping12 import StreamBhadron
from StrippingSettings.Stripping12 import StreamDielectron
from StrippingSettings.Stripping12 import StreamDimuon
from StrippingSettings.Stripping12 import StreamMiniBias
from StrippingSettings.Stripping12 import StreamSemileptonic
from StrippingSettings.Stripping12 import StreamRadiative
from StrippingSettings.Stripping12 import StreamEW
from StrippingSettings.Stripping12 import StreamCharmControl
from StrippingSettings.Stripping12 import StreamCharmFull
from StrippingSettings.Stripping12 import StreamLeptonicFull

allStreams = [
	#        StreamCalibration.stream,
        StreamBhadron.stream,
        StreamDielectron.stream,
        StreamDimuon.stream,
        StreamMiniBias.stream,
        StreamRadiative.stream,
        StreamEW.stream,
        StreamSemileptonic.stream,
        StreamLeptonicFull.stream,
        StreamCharmControl.stream,
        StreamCharmFull.stream,
        ]

stream = StrippingStream("AllStreams")

for s in allStreams:
	stream.appendLines(s.lines)

sc = StrippingConf( Streams = [stream],
                    MaxCandidates = 2000)


stream.sequence().IgnoreFilterPassed = True # so that we get all events written out

from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements)

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'          : stripDSTElements()
    }

SelDSTWriterConf = {
    'default'            : stripDSTStreamConf()
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
DaVinci().InputType = 'DST'
DaVinci().DataType = "2010"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

