'''Global configuration for Stripping28r2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping28r2'
Stripping().TCK = 0x44105282
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
