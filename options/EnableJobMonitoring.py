from Configurables import ApplicationMgr

if "StalledEventMonitoring" in ApplicationMgr.__slots__:
    ApplicationMgr().StalledEventMonitoring = True

if "StopOnSignal" in ApplicationMgr.__slots__:
    ApplicationMgr().StopOnSignal = True
