# Additional options for accessing SQLDDDB conditions DB.
# Add if needed to DIRAC production request, to over-ride DIRAC default for real data
from Configurables import CondDB
CondDB(UseOracle = False)
