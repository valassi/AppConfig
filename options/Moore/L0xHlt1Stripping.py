### @file
#
#   Copy L0xHlt1 accepted events
#
#   This will produce a L0xHlt stripped DST, re-running the L0 
#   with config 0xff68, and Hlt config 0x3ff68 (see HltTCK for
#   more info on this configuration)
#
#   @author P. Koppenburg & G. Raven
#   @date 2009-11-02
# /
import Gaudi.Configuration
#
# Application
#
from Configurables import Moore
Moore().L0 = True
Moore().ReplaceL0BanksWithEmulated = True
Moore().UseTCK = True
# provide an invalid TCK here so one is forced to append eg. Conditions/TCK-0x0005ff68.py 
Moore().InitialTCK = '0x00000000'
Moore().CheckOdin = False
Moore().WriterRequires = [ 'Hlt1Global' ]

