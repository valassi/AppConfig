from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer,RawEventJuggler
from Configurables import LHCbApp
LHCbApp()

MySeq=GaudiSequencer("MoveTCKtoNewLocation")
MyWriter=InputCopyStream()
RawEventJuggler().GenericReplacePatterns={"Averyunlikelystringtohaveinaraweventlocation99": "idontcarebecausethiswillneverbeused"} #BUGFIX: This is needed until LHCb > v36r2 
RawEventJuggler().TCK='0xDEADBEEF'
RawEventJuggler().Input=0.0
RawEventJuggler().Output=3.0
RawEventJuggler().Sequencer=MySeq
RawEventJuggler().WriterOptItemList=MyWriter
RawEventJuggler().KillInputBanksAfter="L0*|Hlt*"
RawEventJuggler().KillExtraNodes=True #remove DAQ/RawEvent completely
ApplicationMgr().TopAlg += [MySeq]
ApplicationMgr().OutStream += [MyWriter]
