from Configurables import Moore
Moore().UseTCK = True
Moore().InitialTCK = '0x00000000'
Moore().L0 = False
Moore().ReplaceL0BanksWithEmulated = False
Moore().UseDBSnapshot = False
Moore().EnableRunChangeHandler = False
Moore().CheckOdin = False
Moore().WriterRequires = []
Moore().Simulation = True
