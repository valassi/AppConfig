# Option file for filtering only releavant events from the PbPb FULL stream
# For details why this is needed refer to
# - https://its.cern.ch/jira/browse/LHCBPS-1480
from Configurables import Brunel
Brunel().Hlt2FilterCode = "HLT_PASS('Hlt2BBPassThroughDecision')"