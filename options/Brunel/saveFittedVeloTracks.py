# options to save all fitted Velo tracks on a DST
# this is a solution to have all Velo tracks for a PV refit on a DST

from Configurables import OutputStream
dstWriter = OutputStream( "DstWriter" )
dstWriter.ItemList += ["/Event/pRec/Track/FittedHLT1VeloTracks#1"]

from Configurables import PackTrack, TrackToDST, GaudiSequencer
packVelo = PackTrack( name               = "PackTracksFittedVelo",
                      InputName          = "Rec/Track/FittedHLT1VeloTracks",
                      OutputName         = "pRec/Track/FittedHLT1VeloTracks",
                      AlwaysCreateOutput = True )

# this removes all states except 'closestToBeam'
filterVeloStates = TrackToDST("FilterFittedVeloTrackStates")
filterVeloStates.TracksInContainer = "Rec/Track/FittedHLT1VeloTracks"

GaudiSequencer("PackDST").Members += [ filterVeloStates, packVelo ] 

