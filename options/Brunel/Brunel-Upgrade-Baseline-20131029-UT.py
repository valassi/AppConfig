## ############################################################################
## # File for running Brunel with Upgrade database
## #
## # Upgrade Detectors: VP_UVP UT FT_MonoLayer Rich1Pmt Rich2Pmt Calo_NoSpdPrs Muon_NoM1
## #
## # Syntax is:
## #   gaudirun.py Brunel-Upgrade-Baseline-20131029-UT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True
CondDB().AllLocalTagsByDataType = ["VP_UVP+RICH_2019+UT", "FT_StereoAngle5", "Muon_NoM1", "Calo_NoSPDPRS"]


from Configurables import Brunel
Brunel().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]
Brunel().DataType     = "Upgrade" 

