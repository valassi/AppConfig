##############################################################################
# File for running Brunel on MC data with default MC09 settings,
# and saving all MC Truth
#
# Syntax is:
# gaudirun.py Brunel/MC09-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Gaudi.Configuration import importOptions
from Configurables import Brunel

importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")
importOptions("$APPCONFIGOPTS/Brunel/DataType-MC09.py")

##############################################################################
