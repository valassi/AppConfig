from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Brunel/Upgrade-WithTruth.py")
from Configurables import Brunel
Brunel().OutputType = "XDST"
for spill in ["Prev","PrevPrev","Next"]:
    OutputStream( "DstWriter").OptItemList += [
        "/Event/%s/MC/PuVeto/Hits#1"%spill
        , "/Event/%s/MC/Velo/Hits#1"%spill
        , "/Event/%s/MC/TT/Hits#1"%spill
        , "/Event/%s/MC/IT/Hits#1"%spill
        , "/Event/%s/MC/OT/Hits#1"%spill
        , "/Event/%s/MC/Rich/Hits#1"%spill
        , "/Event/%s/MC/Prs/Hits#1"%spill
        , "/Event/%s/MC/Spd/Hits#1"%spill
        , "/Event/%s/MC/Ecal/Hits#1"%spill
        , "/Event/%s/MC/Hcal/Hits#1"%spill
        , "/Event/%s/MC/Muon/Hits#1"%spill ]

