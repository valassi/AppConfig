# options to enlarge radial cut for PV fitting
# to be used for PbPb
from Gaudi.Configuration import *

def changePVReco():
    from Configurables import PatPV3D, PVOfflineTool
    from GaudiKernel.SystemOfUnits import mm
    pvAlg = PatPV3D("PatPV3D")
    pvAlg.addTool(PVOfflineTool,"PVOfflineTool")
    pvAlg.PVOfflineTool.UseBeamSpotRCut = True
    pvAlg.PVOfflineTool.BeamSpotRCut = 0.4*mm
    
appendPostConfigAction( changePVReco )
