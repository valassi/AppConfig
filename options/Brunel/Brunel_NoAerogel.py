
importOptions("$APPCONFIGOPTS/Conditions/RichAerogelRemoveInDB.py")


from Configurables import RichRecSysConf,  RichRecQCConf
rConf = RichRecSysConf("RichOfflineRec")
rConf.Radiators = [ "Rich1Gas", "Rich2Gas" ]

rMoni = RichRecQCConf("OfflineRichMoni")
rMoni.Radiators = rConf.Radiators



