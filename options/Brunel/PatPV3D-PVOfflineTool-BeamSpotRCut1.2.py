# options to enlarge radial cut for PV fitting
# to be used for pA
from Gaudi.Configuration import *

def changeRCut():
    from Configurables import PatPV3D, PVOfflineTool
    pvAlg = PatPV3D("PatPV3D")
    pvAlg.addTool(PVOfflineTool,"PVOfflineTool")
    pvAlg.PVOfflineTool.BeamSpotRCut = 1.2
    
appendPostConfigAction( changeRCut )
