##############################################################################
# File for running Brunel on MC09 data in stripping workflow,
# starting from strip ETC
#
# Syntax is:
# gaudirun.py Brunel/MC09-Stripping.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Gaudi.Configuration import importOptions
from Configurables import Brunel

importOptions("$APPCONFIGOPTS/Brunel/MC-Stripping.py")
importOptions("$APPCONFIGOPTS/Brunel/DataType-MC09.py")

###############################################################################
# Example of input ETC file definition to be provided in <someDataFiles>.py
###############################################################################
#from Gaudi.Configuration import EventSelector
#EventSelector().Input = [ "COLLECTION='TagCreator/EventTuple' DATAFILE='"SomeETC.root"' TYP='POOL_ROOT' SEL='(StrippingGlobal==1)'" ]
