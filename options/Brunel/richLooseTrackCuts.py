# Select 'all' tracks to be made into ProtoParticles, useful in early data ...
from Configurables import  GlobalRecoConf
GlobalRecoConf().TrackTypes = [ "Long","Upstream","Downstream","Ttrack","Velo","VeloR" ]
GlobalRecoConf().TrackCuts  = { }
# Loosen RICH track selection cuts for early data
from Configurables import RichTrackCreatorConfig
RichTrackCreatorConfig().TrackCuts = { "Chi2Cut"    : [0,100],
                                        "Likelihood" : [-999999,999999] }
