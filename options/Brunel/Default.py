###############################################################################
# File for running Brunel with default settings (real data,.mdf in,.dst out)
#
# Syntax is:
# gaudirun.py Brunel/Defaults.py Conditions/<someTag>.py <someDataFiles>.py
###############################################################################

from Configurables import Brunel

# Just instantiate the configurable...
theApp = Brunel()

###############################################################################
