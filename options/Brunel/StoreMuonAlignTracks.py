from Gaudi.Configuration import importOptions
from Configurables import GaudiSequencer, TrackToDST
importOptions("$MUONTRACKALIGNROOT/options/MuonTrackAlign.opts")
muonTrackToDST = TrackToDST('MuonTrackToDST',
                            TracksInContainer = 'Rec/Muon/MuonsForAlignment',
                            StoreAllStates = True)
GaudiSequencer('OutputDSTSeq').Members.append(muonTrackToDST)

