from Configurables import OutputStream
writer = OutputStream( "DstWriter" )
writer.OptItemList += [ "/Event/Rec/Track/Downstream#1" ]
writer.OptItemList += [ "/Event/Rec/Track/Seed#1" ]
writer.OptItemList += [ "/Event/Rec/Track/Forward#1" ]
writer.OptItemList += [ "/Event/Rec/Track/Match#1" ]
writer.OptItemList += [ "/Event/Rec/Track/Velo#1" ]
