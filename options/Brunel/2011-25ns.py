##############################################################################
# File for running Brunel on 2011 25ns data that had no Hlt2 line
##############################################################################

from Configurables import Brunel
Brunel().DataType  = "2011"
Brunel().VetoHltErrorEvents = False

##############################################################################
