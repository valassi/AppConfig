##############################################################################
# File for rerunning (typically newer version of) Brunel on MC XDST.
##############################################################################

from Brunel.Configuration import *

Brunel().InputType="XDST"
Brunel().SplitRawEventInput=4.2
Brunel().WithMC=True

from Configurables import VPClus
VPClus("VPClustering").RawEventLocation = "/Event/Velo/RawEvent"
from Configurables import VeloClusterTrackingSIMD
VeloClusterTrackingSIMD("VeloClusterTracking").RawEventLocation = "/Event/Velo/RawEvent"
