##############################################################################
# File for removing aerogel from RICH reco for Upgrade data produced with no aerogel
# ############################################################################
from Configurables import RichRecSysConf
RichRecSysConf().Radiators = [ "Rich1Gas", "Rich2Gas" ]
# ############################################################################

