##############################################################################
# File for running Brunel on Run1 data produced with Sim09 and no packing
#
# It must be passed AFTER that specifying the output type!!!
##############################################################################

from Gaudi.Configuration import *

from Configurables import Brunel

from Configurables import GaudiSequencer, SimConf
packSeq = GaudiSequencer("PackDST")
SimConf().PackingSequencers[''] = packSeq
outputType = Brunel().getProp('OutputType')
if outputType != 'XDST' :
    SimConf().Detectors = []

def shufflePackDST():
    from Configurables import GaudiSequencer
    packDST_list = GaudiSequencer("PackDST").Members
    nodeKillerIndex = -1
    for p in packDST_list:
        if p.name() == 'MCRichNodeKiller':
            nodeKillerIndex = packDST_list.index(p)
 
 
    if nodeKillerIndex != 1:
        packDST_list.append(packDST_list.pop(nodeKillerIndex))
    GaudiSequencer("PackDST").Members = packDST_list
 
appendPostConfigAction(shufflePackDST)

