##############################################################################
# File for running Brunel on MC data with VeloPix Upgrade settings,
# and saving all MC Truth
#
# Syntax is:
# gaudirun.py Brunel/Upgrade-VeloPix-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Brunel

Brunel().DataType  = "Upgrade"
Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().WithMC    = True   # implies also Brunel().Simulation = True

##############################################################################
from Configurables import TrackSys,RecSysConf,RecMoniConf,DstConf
TrackSys().TrackPatRecAlgorithms = ["VeloPix","Forward","TsaSeed","Match","Downstream","VeloPixTT"]
RecSysConf().RecoSequence = ["Decoding", "VELOPIX","TT","IT","OT","Tr","Vertex","RICH","CALO","PROTO","MUON"]
RecMoniConf().MoniSequence=["CALO","OT","ST","PROTO","TT","IT"]
Brunel().MCCheckSequence = ["Pat","TT","IT","OT","CALO","PROTO"]
Brunel().MCLinksSequence = []

