from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 2

from Configurables import CondDB
CondDB().Upgrade     = True

from Configurables import Brunel
Brunel().Detectors  = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Muon', 'Magnet', 'Tr' ]
Brunel().DataType   = "Upgrade"
Brunel().Simulation = True
Brunel().WithMC     = True
Brunel().InputType  = "DIGI"
Brunel().RecoSequence = ["Decoding","TrFast","TrBest","RICH","MUON","SUMMARY"]
Brunel().MainSequence = ['ProcessPhase/Reco', 'ProcessPhase/MCLinks']

for det in ["Calo"]:
    if det in Brunel().MCLinksSequence:
        Brunel().MCLinksSequence.remove(det)

