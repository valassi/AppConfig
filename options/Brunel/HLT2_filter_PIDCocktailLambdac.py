from Gaudi.Configuration import *
from Configurables import Brunel
Brunel().Hlt1FilterCode = ""
Brunel().Hlt2FilterCode = "HLT_PASS_RE('Hlt2PIDLb2LcPi.*Decision') | HLT_PASS_RE('Hlt2PIDLb2LcMuNu.*Decision') | HLT_PASS_RE('Hlt2PIDLc2KPPi.*Decision')"
