from Configurables import Tesla 
from Gaudi.Configuration import *
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict

from TurboStreamProd.streams import turbo_streams, turcal_streams
from Configurables import Tesla

Tesla().EnableLineChecker = True

lines=[]
for vals in turbo_streams['2017'].values():
    lines+=vals['lines']
for vals in turcal_streams['2017'].values():
    lines+=vals['lines']

Tesla().TriggerLines = lines
