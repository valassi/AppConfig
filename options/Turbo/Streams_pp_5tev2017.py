"""Configuration for Tesla streaming with 2017 data."""
from Configurables import Tesla
from TurboStreamProd.streams import turbo_streams

# Check that all lines in the Hlt2DecReports are listed in at least one stream,
# ignoring TurboCalib lines which are processed in a separate Tesla production
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = ['.*TurboCalib']
Tesla().Streams = turbo_streams['5tev2017']
