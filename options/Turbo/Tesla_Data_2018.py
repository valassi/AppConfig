"""Configuration for running Tesla over 2018 data.

Requires at least one additional options file defining Tesla().TriggerLines or
Tesla().Streams.
"""
from Configurables import Tesla

Tesla().DataType = '2018'
Tesla().HDRFilter = True
Tesla().InputType = 'RAW'
Tesla().Mode = 'Online'
Tesla().outputSuffix = '.mdst'
Tesla().Simulation = False
