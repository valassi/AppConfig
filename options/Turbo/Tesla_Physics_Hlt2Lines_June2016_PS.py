from Configurables import Tesla 
from Gaudi.Configuration import *
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict, prescaleDict

### Find out the PS lines
prescaleVersion=4
prescaledLines=prescaleDict[prescaleVersion]["CharmHad"]

linesPS = list(prescaledLines.keys())

Tesla().TriggerLines = linesPS

# configuration for prescaled stream
Tesla().Prescale=prescaleVersion
Tesla().PrescaleDict=prescaledLines
