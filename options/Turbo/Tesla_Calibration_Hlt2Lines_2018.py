"""Configuration for Tesla TurCal productions with 2018 data."""
from Configurables import Tesla
from TurboStreamProd.streams import turcal_streams

# Check that all lines in the Hlt2DecReports are listed in at least one stream,
# ignoring Turbo lines which are processed in a separate Tesla production
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = ['.*Turbo']
Tesla().TriggerLines = turcal_streams['2018']['default']['lines']
