from Configurables import Tesla 
from Gaudi.Configuration import *

version='v9r8'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = allLines(prodDict,version,debug=False)

Tesla().TriggerLines = lines
