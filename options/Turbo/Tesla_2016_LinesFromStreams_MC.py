from __future__ import print_function
from Configurables import Tesla 
from Gaudi.Configuration import *
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict

from TurboStreamProd.streams import turbo_streams 
from Configurables import Tesla

#Tesla().ValidateStreams = True
# Turn on Sascha's algorithm ignoring TurboCalib
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = [".*TurboCalib"]

lines=[]
for vals in turbo_streams['2016'].values():
    lines+=vals['lines']

print(lines)
Tesla().TriggerLines = lines
