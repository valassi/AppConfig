"""Configuration for Tesla streaming with 2018 data."""
from Configurables import Tesla
from TurboStreamProd.streams import turbo_streams

# Check that all lines in the Hlt2DecReports are listed in at least one stream,
# ignoring TurboCalib lines which are processed in a separate Tesla production
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = ['.*TurboCalib']
# For the main part of 2018 we disable completely the charmxsec stream as
# the lines are not present in the HLT2 TCK.
Tesla().Streams = {stream: v for stream, v in turbo_streams['2018'].items()
                   if stream != 'charmxsec'}
