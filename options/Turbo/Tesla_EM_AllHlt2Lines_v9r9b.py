from Configurables import Tesla 
from Gaudi.Configuration import *

version='v9r9b_0x00f4014d'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = allLines(prodDict,version,debug=True)

Tesla().TriggerLines = lines
