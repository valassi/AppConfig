"""Configure the Tesla line checker for TurCal input."""
from Configurables import Tesla

# Ignore Turbo lines which are processed in a separate Tesla production
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = ['.*Turbo']
