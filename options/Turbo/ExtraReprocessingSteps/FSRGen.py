from Configurables import DecodeRawEvent, GaudiSequencer, InputCopyStream, LHCbApp, LumiAlgsConf, ApplicationMgr
from GaudiConf import IOExtension, IOHelper
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

writer=InputCopyStream()
# Begin Lumi configuration
lumiSeq = GaudiSequencer("LumiSeq")
#
# Add ODIN decoder to LumiSeq ***
from DAQSys.Decoders import DecoderDB
DecoderDB["HltLumiSummaryDecoder"].deactivate()
CreateODIN=DecoderDB["createODIN"].setup()
#********************************
#
# Main algorithm config
lumiCounters = GaudiSequencer("LumiCounters")
lumiCounters.Members+=[CreateODIN]
#lumiCounters.OutputLevel = 2
lumiSeq.Members = [ lumiCounters ]
LumiAlgsConf().LumiSequencer = lumiCounters
LumiAlgsConf().InputType = "MDF"
#LumiAlgsConf().OutputLevel=2
#
# Filter out Lumi only triggers from further processing, but still write to output
from Configurables import HltRoutingBitsFilter
physFilter = HltRoutingBitsFilter( "PhysFilter", RequireMask = [ 0x0, 0x4, 0x0 ] )
lumiFilter = HltRoutingBitsFilter( "LumiFilter", RequireMask = [ 0x0, 0x2, 0x0 ] )
lumiSeq.Members += [ lumiFilter, physFilter ]
lumiSeq.ModeOR = True
#
from Configurables import RecordStream
FSRWriter = RecordStream( "FSROutputStreamDstWriter")
FSRWriter.OutputLevel = 3
#
# Sequence to be executed if physics sequence not called (nano events)
PhysSeq = GaudiSequencer("PhysicsSeq")
PhysSeq.ModeOR = True
PhysSeq.Members = [ physFilter ]

from DAQSys.Decoders import DecoderDB
Hlt2DecReportsDecoder=DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"].setup()
from Configurables import LoKi__HDRFilter   as HltDecReportsFilter
physf = HltDecReportsFilter  ( 'HLT2PhysFilterStringent'
        , Code = "HLT_TURBOPASS_RE('^Hlt2.*(?!TurboCalib)Decision$')" 
        , Location = Hlt2DecReportsDecoder.OutputHltDecReportsLocation)
###
writer.AcceptAlgs = [physFilter,physf]
writer.RequireAlgs = [physf]
ApplicationMgr().TopAlg += [lumiSeq,Hlt2DecReportsDecoder,PhysSeq,physf]
###
#ApplicationMgr().TopAlg += [writer]
