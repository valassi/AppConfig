"""Enable Stripping-and-microDST-like filtering of MC objects."""
from Configurables import Tesla

Tesla().FilterMC = True
