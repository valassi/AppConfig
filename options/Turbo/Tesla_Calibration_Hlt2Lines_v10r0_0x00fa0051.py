from Configurables import Tesla 
from Gaudi.Configuration import *

version='v10r0_0x00fa0051'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = streamLines(prodDict,version,'PID',debug=True)
lines += streamLines(prodDict,version,'TrackCalib',debug=True)

Tesla().TriggerLines = lines
