from Configurables import Tesla 
from Gaudi.Configuration import *

version='2016_0x21271600'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = streamLines(prodDict,version,'PID',debug=True)
lines += streamLines(prodDict,version,'TrackEff',debug=True)

Tesla().TriggerLines = lines
