from Configurables import Tesla 
from Gaudi.Configuration import *

version='smog'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = allLines(prodDict,version,debug=False)

Tesla().TriggerLines = lines
Tesla().Pack = True
Tesla().InputType = "RAW"
Tesla().DataType = '2015'
Tesla().Mode = 'Online'
Tesla().RawFormatVersion = 0.2
Tesla().Simulation = False
