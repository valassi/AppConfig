from Configurables import Tesla
Tesla().Pack = True
Tesla().InputType = "DST"
Tesla().Simulation = False
Tesla().Mode = 'Offline' 
Tesla().VertRepLoc = 'Hlt2'
Tesla().DataType = '2016'

from Configurables import TrackSys
TrackSys().GlobalCuts = { 'Velo':10000} 

Tesla().EnableLineChecker = True
Tesla().IgnoredLines = [".*Turbo$"]

Tesla().TriggerLines =[
 "Hlt2PIDB2KJPsiEENegTaggedTurboCalib"
,"Hlt2PIDB2KJPsiEEPosTaggedTurboCalib"
,"Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib"
,"Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib"
,"Hlt2PIDD02KPiTagTurboCalib"
,"Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib"
,"Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib"
,"Hlt2PIDDs2KKPiSSTaggedTurboCalib"
,"Hlt2PIDDs2MuMuPiNegTaggedTurboCalib"
,"Hlt2PIDDs2MuMuPiPosTaggedTurboCalib"
,"Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib"
,"Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib"
,"Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalib"
,"Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalib"
,"Hlt2PIDKs2PiPiLLTurboCalib"
,"Hlt2PIDLambda2PPiLLTurboCalib"
,"Hlt2PIDLambda2PPiLLhighPTTurboCalib"
,"Hlt2PIDLambda2PPiLLisMuonTurboCalib"
,"Hlt2PIDLambda2PPiLLveryhighPTTurboCalib"
,"Hlt2PIDLb2LcMuNuTurboCalib"
,"Hlt2PIDLb2LcPiTurboCalib"
,"Hlt2PIDLc2KPPiTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamMinusHighStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamMinusHighStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamMinusLowStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamMinusLowStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamPlusHighStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamPlusHighStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamPlusLowStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonDownstreamPlusLowStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTMinusHighStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTMinusHighStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTMinusLowStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTMinusLowStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTPlusHighStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTPlusHighStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTPlusLowStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonMuonTTPlusLowStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonMinusHighStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonMinusHighStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonMinusLowStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonMinusLowStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonPlusHighStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonPlusHighStatTaggedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonPlusLowStatMatchedTurboCalib"
,"Hlt2TrackEffDiMuonVeloMuonPlusLowStatTaggedTurboCalib"
]


