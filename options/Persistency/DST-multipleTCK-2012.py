from __future__ import print_function
from Gaudi.Configuration import *

from Configurables import OutputStream

dstWriter = OutputStream("DstWriter")
dstWriter.OptItemList.append('/Event/Trigger/0x4097003d/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x40990042/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x409f0045/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x40a30044/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x40ac0046/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x4094003d/RawEvent#1')
print('# UPDATED: DstWriter.ItemList =', dstWriter.OptItemList)

