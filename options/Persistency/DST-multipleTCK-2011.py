from __future__ import print_function
from Gaudi.Configuration import *

from Configurables import OutputStream

dstWriter = OutputStream("DstWriter")
dstWriter.OptItemList.append('/Event/Trigger/0x40790038/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x40730035/RawEvent#1')
dstWriter.OptItemList.append('/Event/Trigger/0x40760037/RawEvent#1')
print('# UPDATED: DstWriter.ItemList =', dstWriter.OptItemList)

