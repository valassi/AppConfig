# File for setting Beam conditions for MC09
#
# Beam5TeV
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/MC09-b3,5TeV-md100-nu1.py
#              $APPCONFIGOPTS/Conditions/MC09-20090402-vc-md100.py
#              $DECFILESROOT/options/30000000.opts (ie. event type)
#              $LBPYTHIAROOT/options/Pythia.opts (i.e. production engine)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Gauss.Configuration import *

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 0.123*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().CrossingRate      = 11.245*SystemOfUnits.kilohertz
Gauss().TotalCrossSection = 91.1*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().InteractionSize = [ 0.032*SystemOfUnits.mm, 0.032*SystemOfUnits.mm,
                            3.82*SystemOfUnits.cm ]
Gauss().BeamSize        = [ 0.045*SystemOfUnits.mm, 0.045*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance
Gauss().BeamMomentum      = 3.5*SystemOfUnits.TeV
Gauss().BeamCrossingAngle = 0.270*SystemOfUnits.mrad
Gauss().BeamEmittance     = 1.01*(10**(-9))*SystemOfUnits.rad*SystemOfUnits.m
Gauss().BeamBetaStar      = 2.0*SystemOfUnits.m

#--Starting time, to identify beam conditions, all events will have the same
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 301.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

