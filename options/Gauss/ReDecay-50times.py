#-- Activates the redecay of particles options with 50 redecay per original
#-- event. By default, every particle stable in the production tool which
#-- is heavier than the signal particle is redecayed.

from configurables import gauss

gauss().redecay['active'] = true
gauss().redecay['n'] = 50
