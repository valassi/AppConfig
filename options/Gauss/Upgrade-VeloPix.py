# $Id: Upgrade.py,v 1.1 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Gauss with Upgrade VeloPix configuration
##############################################################################

from Gauss.Configuration import *
Gauss().DataType = "Upgrade"

#  VeloPix instead of Velo 
Gauss().DetectorSim["VELO"]=['VeloPix']
Gauss().DetectorMoni["VELO"]=['VeloPix']
Gauss().DetectorGeo["VELO"]=['VeloPix']



