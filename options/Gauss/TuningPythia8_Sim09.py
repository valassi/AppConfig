from Configurables import Generation
from Configurables import MinimumBias
from Configurables import Inclusive
from Configurables import ( SignalPlain, SignalRepeatedHadronization, Special )
from Configurables import Pythia8Production


gaussGen = Generation("Generation")

gaussGen.addTool(MinimumBias, name = "MinimumBias")
gaussGen.MinimumBias.ProductionTool = "Pythia8Production"
gaussGen.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.MinimumBias.Pythia8Production.Tuning = ""
gaussGen.MinimumBias.Pythia8Production.UserTuning = "$APPCONFIGOPTS/Gauss/tunings/LHCbDefault_Gauss_v53r0.cmd"

gaussGen.addTool(Inclusive, name = "Inclusive")
gaussGen.Inclusive.ProductionTool = "Pythia8Production"
gaussGen.Inclusive.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.Inclusive.Pythia8Production.Tuning = ""
gaussGen.Inclusive.Pythia8Production.UserTuning = "$APPCONFIGOPTS/Gauss/tunings/LHCbDefault_Gauss_v53r0.cmd"

gaussGen.addTool(SignalPlain, name = "SignalPlain")
gaussGen.SignalPlain.ProductionTool = "Pythia8Production"
gaussGen.SignalPlain.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.SignalPlain.Pythia8Production.Tuning = ""
gaussGen.SignalPlain.Pythia8Production.UserTuning = "$APPCONFIGOPTS/Gauss/tunings/LHCbDefault_Gauss_v53r0.cmd"

gaussGen.addTool(SignalRepeatedHadronization, name = "SignalRepeatedHadronization")
gaussGen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
gaussGen.SignalRepeatedHadronization.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.SignalRepeatedHadronization.Pythia8Production.Tuning = ""
gaussGen.SignalRepeatedHadronization.Pythia8Production.UserTuning = "$APPCONFIGOPTS/Gauss/tunings/LHCbDefault_Gauss_v53r0.cmd"

gaussGen.addTool(Special, name = "Special")
gaussGen.Special.ProductionTool = "Pythia8Production"
gaussGen.Special.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.Special.Pythia8Production.Tuning = ""
gaussGen.Special.Pythia8Production.UserTuning = "$APPCONFIGOPTS/Gauss/tunings/LHCbDefault_Gauss_v53r0.cmd"

