# Manually traversing the GaudiSequencer structure to set the RootInTES of
# all members of a sequence if the sequencer itself had a RootInTES set. This
# acts as a temporarily fix to changes in Gaudi.
#
# Affected versions: Gauss v53r2

from Gaudi.Configuration import appendPostConfigAction
from Gaudi.Configuration import GaudiSequencer
from Gaudi.Configuration import ApplicationMgr

def propagateRootInTES(head_seq, rootInTes=''):
    if head_seq.getProp('RootInTES') != '':
        current_root = head_seq.getProp('RootInTES')
    else:
        current_root = rootInTes

    for alg in head_seq.Members:
        if type(alg) == GaudiSequencer:
            propagateRootInTES(alg, current_root)
        else:
            alg.RootInTES = current_root

def fixRootInTESAction():
    for alg in ApplicationMgr().TopAlg:
        propagateRootInTES(alg)

appendPostConfigAction(fixRootInTESAction)
