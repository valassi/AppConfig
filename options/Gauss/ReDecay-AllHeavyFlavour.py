#-- Changes the redecay mode to redecay the signal and all particles of at least
#-- the signal's flavour
#-- Note: Not yet released as of 9/4/2019. Likely in v49r13 for Sim09

#-- Note: this option file alone does not turn on redecay!

from Configurables import Gauss
Gauss().Redecay['rd_mode'] = 3  # From default 1
