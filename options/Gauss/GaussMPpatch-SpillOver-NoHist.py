from __future__ import print_function
# ---------------------------------------------------------------------------


class GaussMPpatch_1409(object):

    # Workarounds for LHCBGAUSS-1409
    # This patch is necessary for all Gauss versions tested so far (>= v51r0)
    @staticmethod
    def postConfigActions():
        pid = '(GaussMPpatch)'
        print(pid, '--- Workarounds for LHCBGAUSS-1409 (start) ---')
        from Configurables import ApplicationMgr
        print(pid, 'Disable StalledEventMonitoring')
        ApplicationMgr().StalledEventMonitoring = False
        print(pid, '--- Workarounds for LHCBGAUSS-1409 (end)   ---')

    # Apply the workarounds for LHCBGAUSS-1409
    @staticmethod
    def apply():
        pid = '(GaussMPpatch)'
        print(pid, '--- Workarounds for LHCBGAUSS-1409 (start) ---')
        print(pid, 'Append post-config action to disable StalledEventMonitoring')
        from Gaudi.Configuration import appendPostConfigAction
        appendPostConfigAction(GaussMPpatch_1409.postConfigActions)
        print(pid, '--- Workarounds for LHCBGAUSS-1409 (end)   ---')

# ---------------------------------------------------------------------------


class GaussMPpatch_1779_1780_1785_1811_1814(object):

    # Workarounds for LHCBGAUSS-1779
    # This is a patch over the source code of Gaudi v30r3 (used by Gauss v53r1)
    # It is necessary for Gauss v52r0 and above
    # It is not necessary, but harmless, for Gauss v51r0 and below (no '/FileRecords/GenFSR' expected)
    @staticmethod
    def SendFileRecords(self):
        # send the FileRecords data as part of finalisation
        # Take Care of FileRecords!
        # There are two main things to consider here
        # 1) The DataObjects in the FileRecords Transient Store
        # 2) The fact that they are Keyed Containers, containing other objects
        #
        # The Lead Worker, nodeID=0, sends everything in the FSR store, as
        #   a completeness guarantee,
        #
        # send in form ( nodeID, path, object)
        pid = '[%s-%i] (GaussMPpatch)' % (self._gmpc.nodeType,
                                          self._gmpc.nodeID)
        print(pid, 'Entering FileRecordsAgent.SendFileRecords from GaussMPpatch (workarounds for LHCBGAUSS-1779)')
        from GaudiPython import SUCCESS
        import pickle
        self.log.info('Sending FileRecords...')
        lst = self.fsr.getHistoNames()
        # Check Validity
        if not lst:
            self.log.info('No FileRecords Data to send to Writer.')
            self.q.put('END_FSR')
            return SUCCESS
        # no need to send the root node
        if '/FileRecords' in lst:
            lst.remove('/FileRecords')
        for l in lst:
            o = self.fsr.retrieveObject(l)
            if hasattr(o, 'configureDirectAccess'):
                o.configureDirectAccess()
            # lead worker sends everything, as completeness guarantee
            if self._gmpc.nodeID == 0:
                self.objectsOut.append((0, l, pickle.dumps(o)))
            else:
                # only add the Event Counter
                # and non-Empty Keyed Containers (ignore empty ones)
                if l == '/FileRecords/EventCountFSR':
                    tup = (self._gmpc.nodeID, l, pickle.dumps(o))
                    self.objectsOut.append(tup)
                elif l == '/FileRecords/GenFSR':
                    print(pid, '--- Workarounds for LHCBGAUSS-1779 (start) ---')
                    print(pid, 'WARNING! Ignoring %s (%s)' % (
                        l, o.__class__.__name__))
                    self.log.warning('Ignoring %s' % l)
                    print(pid, '--- Workarounds for LHCBGAUSS-1779 (end)   ---')
                elif 'KeyedContainer' not in o.__class__.__name__:
                    print(pid, '--- Workarounds for LHCBGAUSS-1779 (start) ---')
                    print(pid, 'WARNING! Ignoring %s (%s) because it is not a KeyedContainer' % (
                        l, o.__class__.__name__))
                    self.log.warning('Ignoring %s' % l)
                    print(pid, '--- Workarounds for LHCBGAUSS-1779 (end)   ---')
                else:
                    # It's a Keyed Container
                    assert 'KeyedContainer' in o.__class__.__name__
                    nObjects = o.numberOfObjects()
                    if nObjects:
                        self.log.debug('Keyed Container %s with %i objects'
                                       % (l, nObjects))
                        tup = (self._gmpc.nodeID, l, pickle.dumps(o))
                        self.objectsOut.append(tup)
        self.log.debug('Done with FSR store, just to send to Writer.')
        if self.objectsOut:
            self.log.debug('%i FSR objects to Writer' % (len(self.objectsOut)))
            for ob in self.objectsOut:
                self.log.debug('\t%s' % (ob[0]))
            self.q.put(self.objectsOut)
        else:
            self.log.info('Valid FSR Store, but no data to send to Writer')
        self.log.info('SendFSR complete')
        self.q.put('END_FSR')
        return SUCCESS

    # -----------------------------------------------------------------------

    # Workarounds for LHCBGAUSS-1780/1785
    # This is a patch over the source code of Gaudi v30r3 (used by Gauss v53r1)
    # This patch is necessary for Gauss v53r1 and above
    # A part of it should be avoided (the rest being unnecessary but harmless) for Gauss v52r2 and below
    # It is pointless for Gauss v53r0, where there is no workaround for LHCBGAUSS-1780
    class EvtCounterWrapper(object):
        def __init__(self, gmpc):
            self.gmpc = gmpc
            if self.gmpc.nodeID < 0:
                pid = '[%s] (GaussMPpatch)' % (self.gmpc.nodeType)
            else:
                pid = '[%s-%i] (GaussMPpatch)' % (self.gmpc.nodeType,
                                                  self.gmpc.nodeID)
            print(pid, 'Creating an EvtCounter wrapper (workarounds for LHCBGAUSS-1780/1785)')
            # Print out the meaning of "Nr. in job"
            if self.gmpc.nodeID >= 0:  # no printout needed for Reader and Writer
                from Configurables import LbAppInit, GenInit
                if hasattr(LbAppInit, 'EvtCounter'):
                    # This is v52r2 or earlier
                    print(pid, 'Property EvtCounter exists in LbAppInit: old Gauss version <= v52r2 (not affected by bug LHCBGAUSS-1785)')
                    print(pid, 'NB: "Nr. in job" represents the event number across all workers (as expected)')
                else:
                    # This is v53r0 or later
                    print(pid, 'Property EvtCounter does not exist in LbAppInit: new Gauss version >= v53r0 (affected by bug LHCBGAUSS-1785)')
                    print(pid, 'NB: "Nr. in job" represents the event number in each worker, not across all workers')
            # Fetch tool GaussGen.EvtCounter
            self.cntrs = []
            from Configurables import Gauss
            for path in [''] + Gauss().SpilloverPaths:
                tool = 'GaussGen' + path + '.EvtCounter'
                print(pid, 'Fetch tool', tool, '(workaround for LHCBGAUSS-1780)')
                tool = self.gmpc.a.tool(tool)
                from GaudiPython import InterfaceCast, gbl
                self.cntrs.append(InterfaceCast(
                    gbl.IEventCounter)(tool.getInterface()))

        def setEventCounter(self, evt):
            # NB: worker nodeID has changed by now
            pid = '[%s-%i] (GaussMPpatch)' % (self.gmpc.nodeType,
                                              self.gmpc.nodeID)
            print(pid, 'New event: number in job across all workers = %s' % evt)
            for cntr in self.cntrs:
                cntr.setEventCounter(evt)

    # -----------------------------------------------------------------------

    # Workarounds for LHCBGAUSS-1780/1785
    # This is a patch over the source code of Gaudi v30r3 (used by Gauss v53r1)
    # It is necessary for Gauss v53r1 and above
    # It is not necessary, but harmless, for Gauss v52r2 and below (no need to look for GaussGen.EvtCounter, as ToolSvc.EvtCounter exists)
    # *** No workaround for this issue is available for Gauss v53r0 ***
    @staticmethod
    def Initialize(self):
        if self.nodeID < 0:
            pid = '[%s] (GaussMPpatch)' % (self.nodeType)
        else:
            pid = '[%s-%i] (GaussMPpatch)' % (self.nodeType, self.nodeID)
        print(pid, 'Entering GMPComponent.Initialize from GaussMPpatch (workarounds for LHCBGAUSS-1780/1785)')
        from GaudiPython import InterfaceCast, gbl
        import time
        start = time.time()
        self.processConfiguration()
        self.SetupGaudiPython()
        self.initEvent.set()
        self.StartGaudiPython()
        if self.app == 'Gauss':
            print(pid, 'Fetch tool ToolSvc.EvtCounter')
            tool = self.a.tool('ToolSvc.EvtCounter')
            self.cntr = InterfaceCast(gbl.IEventCounter)(tool.getInterface())
            if self.cntr is None:
                print(pid, '--- Workarounds for LHCBGAUSS-1780/1785 (start) ---')
                print(pid, 'IEventCounter is still:', self.cntr)
                self.cntr = GaussMPpatch_1779_1780_1785_1811_1814.EvtCounterWrapper(
                    self)
                print(pid, '--- Workarounds for LHCBGAUSS-1780/1785 (end)   ---')
            print(pid, 'IEventCounter is now:', self.cntr)
        else:
            self.cntr = None
            print(pid, 'IEventCounter is None')
        self.iTime = time.time() - start

    # -----------------------------------------------------------------------

    # Workarounds for LHCBGAUSS-1780/1785
    # This is a wrapper over the released version of Start
    # It is only needed when EvtCounterWrapper is used (to fix the worker id printout)
    @staticmethod
    def Start(self):
        if self.nodeID < 0:
            pid = '[%s] (GaussMPpatch)' % (self.nodeType)
        else:
            pid = '[%s-%i] (GaussMPpatch)' % (self.nodeType, self.nodeID)
        print(pid, 'Entering GMPComponent.Start from GaussMPpatch (workarounds for LHCBGAUSS-1780/1785)')
        if self.nodeID > 0 and isinstance(self.cntr, GaussMPpatch_1779_1780_1785_1811_1814.EvtCounterWrapper):
            self.cntr.gmpc = self  # EvtCounterWrapper will print Worker-N instead of Worker-0
        return self.StartOLD()

    # -----------------------------------------------------------------------

    # Workarounds for LHCBGAUSS-1814
    # Dummy version of dumpHistograms bypassing the handling of histograms
    @staticmethod
    def dumpHistograms(self):
        if self.nodeID < 0:
            pid = '[%s] (GaussMPpatch)' % (self.nodeType)
        else:
            pid = '[%s-%i] (GaussMPpatch)' % (self.nodeType, self.nodeID)
        print(pid, 'Entering GMPComponent.dumpHistograms from GaussMPpatch (workarounds for LHCBGAUSS-1814)')
        return {}

    # -----------------------------------------------------------------------

    # Dummy replacement for module GaudiMP.GMPBase
    # This was developed against the source code of Gaudi v30r3 (used by Gauss v53r1)
    class GaudiMP(object):
        class GMPBase(object):
            @staticmethod
            def Coord(nWorkers, config, log):
                pid = '(GaussMPpatch)'
                print(pid, '--- Workarounds for LHCBGAUSS-1779/1780/1785/1811/1814 (start) ---')
                print(pid, 'Entering Coord from GaussMPpatch (workarounds for LHCBGAUSS-1779/1780/1785/1811/1814)')
                # Delete GaudiMP and GaudiMP.GMPBase dummy modules
                print(pid, 'Delete GaudiMP and GaudiMP.GMPBase dummy modules')
                import sys
                del sys.modules['GaudiMP']
                del sys.modules['GaudiMP.GMPBase']
                # Override pTools.FileRecordsAgent.SendFileRecords
                print(pid, 'Override pTools.FileRecordsAgent.SendFileRecords (workarounds for LHCBGAUSS-1779)')
                from GaudiMP.pTools import FileRecordsAgent
                FileRecordsAgent.SendFileRecords = GaussMPpatch_1779_1780_1785_1811_1814.SendFileRecords
                # Override GMPBase.GMPComponent.Initialize
                print(pid, 'Override GMPBase.GMPComponent.Initialize (workarounds for LHCBGAUSS-1780/1785)')
                from GaudiMP import GMPBase
                GMPBase.GMPComponent.Initialize = GaussMPpatch_1779_1780_1785_1811_1814.Initialize
                # Override GMPBase.GMPComponent.Start
                # (this is only needed to fix the printout, "[Worker-1]" instead of "[Worker-0]")
                print(pid, 'Override GMPBase.GMPComponent.Start (workarounds for LHCBGAUSS-1780/1785)')
                GMPBase.GMPComponent.StartOLD = GMPBase.GMPComponent.Start
                GMPBase.GMPComponent.Start = GaussMPpatch_1779_1780_1785_1811_1814.Start
                # Override GMPBase.GMPComponent.dumpHistograms
                print(pid, 'Override GMPBase.GMPComponent.dumpHistograms (workarounds for LHCBGAUSS-1814)')
                GMPBase.GMPComponent.dumpHistograms = GaussMPpatch_1779_1780_1785_1811_1814.dumpHistograms
                # Return a Coord from the original GaudiMP.GMPBase
                import GaudiMP.GMPBase as gpp
                print(pid, 'Create a Coord from the original GaudiMP.GMPBase')
                Parall = gpp.Coord(nWorkers, config, log)
                # Override global parameters defined in GMPBase.py
                print(pid, 'Override global parameters defined in GMPBase.py (workarounds for LHCBGAUSS-1811)')
                WAIT_INITIALISE = 120*60  # 120 min (default: 5 min)
                WAIT_FIRST_EVENT = 30*60  # 30 min (default: 3 min)
                WAIT_SINGLE_EVENT = 480*60  # 480 min (default: 6 min)
                WAIT_FINALISE = 300*60  # 300 min (default: 2 min)
                STEP_INITIALISE = 1*60  # 1 min (default: 10 sec)
                STEP_EVENT = 1*60  # 1 min (default:  2 sec)
                STEP_FINALISE = 1*60  # 1 min (default: 10 sec)
                print(pid, 'OLD Parall.sInit.limit:', Parall.sInit.limit)
                print(pid, 'OLD Parall.sRun.limitFirst:', Parall.sRun.limitFirst)
                print(pid, 'OLD Parall.sRun.limit:', Parall.sRun.limit)
                print(pid, 'OLD Parall.sFin.limit:', Parall.sFin.limit)
                print(pid, 'OLD Parall.sInit.step:', Parall.sInit.step)
                print(pid, 'OLD Parall.sRun.step:', Parall.sRun.step)
                print(pid, 'OLD Parall.sFin.step:', Parall.sFin.step)
                Parall.sInit.limit = WAIT_INITIALISE
                Parall.sRun.limitFirst = WAIT_FIRST_EVENT
                Parall.sRun.limit = WAIT_SINGLE_EVENT
                Parall.sFin.limit = WAIT_FINALISE
                Parall.sInit.step = STEP_INITIALISE
                Parall.sRun.step = STEP_EVENT
                Parall.sFin.step = STEP_FINALISE
                print(pid, 'NEW Parall.sInit.limit:', Parall.sInit.limit)
                print(pid, 'NEW Parall.sRun.limitFirst:', Parall.sRun.limitFirst)
                print(pid, 'NEW Parall.sRun.limit:', Parall.sRun.limit)
                print(pid, 'NEW Parall.sFin.limit:', Parall.sFin.limit)
                print(pid, 'NEW Parall.sInit.step:', Parall.sInit.step)
                print(pid, 'NEW Parall.sRun.step:', Parall.sRun.step)
                print(pid, 'NEW Parall.sFin.step:', Parall.sFin.step)
                # Dump all available subprocesses
                # (NB: log.name is "Writer--2" for all of them, do not use it!)
                print(pid, 'The available processes in the GaudiMP Coord are the following:')
                for sp in Parall.system:
                    print(pid, sp.nodeType + \
                        ('' if sp.nodeID < 0 else '-%i' % sp.nodeID))
                for sp in Parall.subworkers:
                    print(pid, sp.nodeType + \
                        ('' if sp.nodeID < 0 else '-%i' % sp.nodeID))
                print(pid, '--- Workarounds for LHCBGAUSS-1779/1780/1785/1811/1814 (end)   ---')
                return Parall

    # -----------------------------------------------------------------------

    # Apply the workarounds for LHCBGAUSS-1779/1780/1785/1811/1814
    @staticmethod
    def apply():
        pid = '(GaussMPpatch)'
        # Override module GaudiMP.GMPBase
        print(pid, '--- Workarounds for LHCBGAUSS-1779/1780/1785/1811/1814 (start) ---')
        print(pid, 'Override module GaudiMP.GMPBase')
        import sys
        # ensure 'import GaudiMP.GMPBase as gpp' defines gpp as the patched GaudiMP.GMPBase
        sys.modules['GaudiMP'] = GaussMPpatch_1779_1780_1785_1811_1814.GaudiMP
        sys.modules['GaudiMP.GMPBase'] = 0  # avoid 'No module named GMPBase'
        print(pid, '--- Workarounds for LHCBGAUSS-1779/1780/1785/1811/1814 (end)   ---')

# ---------------------------------------------------------------------------


class GaussMPpatch_1786(object):

    # Workarounds for LHCBGAUSS-1786
    # This patch is necessary for Gauss v53r1 and above
    # It should be avoided for Gauss v52r2 and below
    # It is pointless for Gauss v53r0, where there is no workaround for LHCBGAUSS-1780
    @staticmethod
    def postConfigActions():
        pid = '(GaussMPpatch)'
        print(pid, '--- Workarounds for LHCBGAUSS-1786 (start) ---')
        from Configurables import LbAppInit, GenInit
        if hasattr(LbAppInit, 'EvtCounter'):
            # This is v52r2 or earlier
            print(pid, 'Property EvtCounter exists in LbAppInit: old Gauss version <= v52r2, no need to apply the workaround')
        else:
            # This is v53r0 or later
            print(pid, 'Property EvtCounter does not exist in LbAppInit: new Gauss version >= v53r0, apply the workaround')
            from Configurables import Gauss
            for path in [''] + Gauss().SpilloverPaths:
                gen = 'GaussGen' + path
                print(pid, 'GenInit(%s).FirstEventNumber was' % gen, GenInit(
                    gen).FirstEventNumber)
                GenInit(gen).FirstEventNumber = GenInit(
                    gen).FirstEventNumber - 1
                print(pid, 'GenInit(%s).FirstEventNumber is now' % gen, GenInit(
                    gen).FirstEventNumber)
        print(pid, '--- Workarounds for LHCBGAUSS-1786 (end)   ---')

    # Apply the workarounds for LHCBGAUSS-1786
    @staticmethod
    def apply():
        pid = '(GaussMPpatch)'
        print(pid, '--- Workarounds for LHCBGAUSS-1786 (start) ---')
        print(pid, 'Append post-config action to decrease FirstEventNumber by 1')
        from Gaudi.Configuration import appendPostConfigAction
        appendPostConfigAction(GaussMPpatch_1786.postConfigActions)
        print(pid, '--- Workarounds for LHCBGAUSS-1786 (end)   ---')

# ---------------------------------------------------------------------------


class GaussMPpatch_preV53R1(object):

    # Backward compatibility patch for Gauss v53r0 or earlier
    @staticmethod
    def enablePreV53R1():
        pid = '(GaussMPpatch)'
        from Configurables import Gauss
        if not hasattr(Gauss, 'isGaussMP'):  # backport the patch to Gauss v53r0 and earlier
            # This is v53r0 or earlier
            print(pid, 'Method Gauss.isGaussMP does not exist: this is Gauss <= v53r0')
            print(pid, 'Implement method Gauss.isGaussMP as in Gauss v53r1')
            Gauss.isGaussMP = GaussMPpatch_preV53R1.isGaussMP
            # Check if this is v52r2 or earlier
            from Configurables import LbAppInit, GenInit
            if hasattr(LbAppInit, 'EvtCounter'):
                # This is v52r2 or earlier
                print(pid, 'Property EvtCounter exists in LbAppInit: this is Gauss <= v52r2')
                GaussMPpatch_preV53R1.isGaussV53R0 = False
            else:
                # This is v53r0 or later
                print(pid, 'Property EvtCounter does not exist in LbAppInit: this is Gauss >= v53r0')
                GaussMPpatch_preV53R1.isGaussV53R0 = True
        else:
            # This is v53r1 or later
            print(pid, 'Method Gauss.isGaussMP exists: this is Gauss >= v53r1')
            GaussMPpatch_preV53R1.isGaussV53R0 = False

    # Workaround for missing isGaussMP method in Gauss v53r0 and earlier
    # This is the isGaussMP() implementation from Gauss v53r1
    @staticmethod
    def isGaussMP(self):
        import argparse
        parser = argparse.ArgumentParser(description='Hello')
        parser.add_argument('--ncpus', action='store', type=int, default=0)
        args = parser.parse_known_args()
        return args[0].ncpus != 0

    # Is this Gauss v53r0?
    isGaussV53R0 = False


# ---------------------------------------------------------------------------

print('=== GaussMPpatch (start) ===')
GaussMPpatch_preV53R1.enablePreV53R1()
pid = '(GaussMPpatch)'
from Configurables import Gauss
if Gauss().isGaussMP():
    print(pid, 'This is GaussMP: apply the patch')
    if GaussMPpatch_preV53R1.isGaussV53R0:
        raise Exception(
            'GaussMPpatch: no workaround for LHCBGAUSS-1780 is possible in Gauss v53r0')
    GaussMPpatch_1409.apply()
    GaussMPpatch_1779_1780_1785_1811_1814.apply()
    GaussMPpatch_1786.apply()
else:
    print(pid, 'This is not GaussMP: no need to apply the patch')
print('=== GaussMPpatch (end)   ===')
