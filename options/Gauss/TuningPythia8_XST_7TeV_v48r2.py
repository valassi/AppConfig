from Configurables import Generation
from Configurables import MinimumBias
from Configurables import Inclusive
from Configurables import Pythia8Production



commandsTuning_XST_7TeV = [
    'SigmaTotal:setOwn         = on', 
    'SigmaTotal:sigmaTot       = 9.407715e+01',
    'SigmaTotal:sigmaAX        = 7.560716e+00',
    'SigmaTotal:sigmaXB        = 7.391497e+00',
    'SigmaTotal:sigmaEl        = 2.946597e+01',
    'SigmaTotal:sigmaXX        = 1.530803e+00',
    'SigmaDiffractive:dampen   = off',
    ]

gaussGen = Generation("Generation")
gaussGen.addTool(MinimumBias, name = "MinimumBias")
gaussGen.MinimumBias.ProductionTool = "Pythia8Production"
gaussGen.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")

gaussGen.MinimumBias.Pythia8Production.Commands += commandsTuning_XST_7TeV


gaussGen.addTool(Inclusive, name = "Inclusive")
gaussGen.Inclusive.ProductionTool = "Pythia8Production"
gaussGen.Inclusive.addTool(Pythia8Production, name = "Pythia8Production")

gaussGen.Inclusive.Pythia8Production.Commands += commandsTuning_XST_7TeV


