##############################################################################
# File for running Gauss on 2018 detector configuration
##############################################################################

from Configurables import Gauss
Gauss().DataType  = "2018"
