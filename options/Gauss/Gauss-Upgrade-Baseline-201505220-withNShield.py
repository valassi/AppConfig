############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
# the actual version is goign to be picked up by selecting the dbase tags
# In addition this file includes the geometry of the SciFi neutron Shield
############################################################################

from Configurables import Gauss, CondDB

CondDB().Upgrade = True

Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }

Gauss().DataType = "Upgrade"

# Activate the neutron shield
CondDB().AllLocalTagsByDataType=["FT_NeutronShieldActivate_TypeA"] 

from Gaudi.Configuration import *
def NeutronShieldActivate(): 
    from Configurables import GiGaInputStream 
    Geo = GiGaInputStream("Geo") 
    Geo.StreamItems += ["/dd/Structure/LHCb/DownstreamRegion/NeutronShielding"] 


appendPostConfigAction( NeutronShieldActivate )

