##
##  File containing options to activate the QGSP_BERT Hadronic
##  Physics List in Geant4 (the default for production is 
##  LHEP)
##

from Configurables import Gauss

Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'QGSP_BERT', "GeneralPhys":True, "LHCbPhys":True}
