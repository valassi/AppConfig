# These options correspond to running only the generator phase of Gauss and
# writing the HepMC format and MCParticles/MCVertices filled directly from
# the generator in .xgen (extended generator) files

from Configurables import Gauss

Gauss().Phases = ['Generator', 'GenToMCTree']

