#-- Changes the redecay mode to only decay the signal particle. Faster but
#-- dangerous if the studied decay has a realistic chance of occuring twice
#-- in the same event as the not-signal-flagged candidate will be identical
#-- in all events.

#-- Note: this option file alone does not turn on redecay!

from Configurables import Gauss
Gauss().Redecay['rd_mode'] = 0  # From default 1
