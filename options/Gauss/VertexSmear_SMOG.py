from Configurables import Generation
from Configurables import FlatZSmearVertex
Generation("Generation").VertexSmearingTool = "FlatZSmearVertex"
Generation("Generation").addTool( FlatZSmearVertex )
Generation("Generation").FlatZSmearVertex.ZMin = -250. #Range in mm in which SMOG analyses will be performed
Generation("Generation").FlatZSmearVertex.ZMax = 250.
