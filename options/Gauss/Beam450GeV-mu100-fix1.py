# File for setting Beam conditions for MC09 for beam of 450 GeV and magnet off
#
# Beam450GeV
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam450GeV-mu100-fix1.py
#              $APPCONFIGOPTS/Conditions/XXXX-YYYY-md100.py
#              $DECFILESROOT/options/30000000.opts (ie. event type)
#              $LBPYTHIAROOT/options/Pythia.opts (i.e. production engine)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Gauss.Configuration import *

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 0.172*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().CrossingRate      = 11.245*SystemOfUnits.kilohertz
Gauss().TotalCrossSection = 65.3*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type

Gauss().InteractionSize = [ 0.198*SystemOfUnits.mm, 0.198*SystemOfUnits.mm,
                            5.01*SystemOfUnits.cm ]
Gauss().BeamSize        = [ 0.280*SystemOfUnits.mm, 0.280*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance
Gauss().BeamMomentum      = 450.0*SystemOfUnits.GeV
Gauss().BeamCrossingAngle = 2.1*SystemOfUnits.mrad
Gauss().BeamEmittance     = 7.835*(10**(-9))*SystemOfUnits.rad*SystemOfUnits.m
Gauss().BeamBetaStar      = 10.0*SystemOfUnits.m

#--Starting time, to identify beam conditions, all events will have the same
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 111.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  =   0.0

#-- Fix one interaction per bunch crossing!
from Configurables import Generation
Generation("Generation").PileUpTool = "FixedNInteractions"
