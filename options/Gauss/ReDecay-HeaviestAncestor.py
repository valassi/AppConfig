#-- Changes the redecay mode to redecay the heaviest ancestor of the signal
#-- particle. The signal particle is identified using the signal vertex as
#-- marked by the DecayTool.

#-- Note: this option file alone does not turn on redecay!

from Configurables import Gauss
Gauss().Redecay['rd_mode'] = 2  # From default 1
