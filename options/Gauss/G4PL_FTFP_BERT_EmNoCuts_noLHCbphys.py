##
##  File containing options to activate the FTFP_BERT Hadronic
##  Physics List in Geant4 (the default for production is
##  LHEP) and deactivate the simulation of Cherenkov photons.
##

from Configurables import Gauss

Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'FTFP_BERT', "GeneralPhys":True, "LHCbPhys":False}
