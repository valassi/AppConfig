# These options correspond to running only the generator phase of Gauss and
# writing the HepMC format in .gen files

from Configurables import Gauss

Gauss().Phases = ["Generator"]
