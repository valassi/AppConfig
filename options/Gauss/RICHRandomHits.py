##############################################################################
# File for switching on RICH random hits, Requires Gauss v47r0 or higher
##############################################################################

from Configurables import Gauss
Gauss().RICHRandomHits = True

##############################################################################
