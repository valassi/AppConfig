from Configurables import Generation
from Configurables import FlatZSmearVertex 
Generation("Generation").VertexSmearingTool = "FlatZSmearVertex"
Generation("Generation").addTool( FlatZSmearVertex ) 
Generation("Generation").FlatZSmearVertex.ZMin = -1000. 
Generation("Generation").FlatZSmearVertex.ZMax =   300.
