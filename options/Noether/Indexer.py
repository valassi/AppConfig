##
## Job options to produce an index file for the events in the input files
##

from Gaudi.Configuration import *
from Configurables import LHCbApp, EventIndexer
from Configurables import DstConf, UnpackDecReport
DstConf().EnableUnpack = ['Reconstruction', 'Stripping']
decConv = UnpackDecReport("Strip_pPhys_DecReports_Converter")
decConv.InputName = "Strip/pPhys/DecReports"

from Configurables import ApplicationMgr, DataOnDemandSvc
ApplicationMgr().ExtSvc.append(DataOnDemandSvc())

DstConf().EnableUnpack = ['Reconstruction', 'Stripping']

LHCbApp()

ApplicationMgr().TopAlg += [EventIndexer()]

EventSelector().PrintFreq = 10000

## Provide one or more input files.
# EventSelector().Input = [
#  "DATAFILE='PFN:MyInputFile.dst' TYP='POOL_ROOTTREE' OPT='READ'"
# ]

## Define the output file
# EventIndexer().OutputFile = "index.root"
# EventIndexer().Stripping = "StrippingXYZ"
