2018-12-11 AppConfig v3r373
========================================

This version is released on 'master' branch.

### New features

- Beam parameters for Gauss corresponding to 2018 data taking, !53 (@kreps) [LHCBGAUSS-1532]  
  Scripts for setting beam parameters in Gauss for 2018 MC. Based on LHCBGAUSS-1532.

- Options for 2018 pp MCTCK, !52 (@dtou) [LHCBGAUSS-1532]

  Created the MC TCKs for 2018 pp collisions. They are 0x517A18A4 for HLT1 and 0x617D18A4 for HLT2. Option for their corresponding L0TCK has 0x18A4 been added. Needed by 2018 validation.
