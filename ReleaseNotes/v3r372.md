
2018-11-20 AppConfig v3r372
========================================

### New features


- Add Gauss options for very low mu 2017 run, see LHCBGAUSS-1481, !50 (@gcorti) [LHCBGAUSS-1481]  

- Add L0AppTCK-0x1725.py, !49 (@baudurie) [TCK-0]  
  Add L0 TCK option file for p-Ne production model
