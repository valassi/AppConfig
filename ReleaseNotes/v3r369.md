2018-10-02 AppConfig v3r369
========================================

- Options for fixing 2017 VeloMuon tracking efficiency lines. !42 (@dtou)   
  Added options for MCTCK `0x62661709` to fix 2017 VeloMuon tracking efficiency lines in `0x61661709`, see LBHLT-455.
