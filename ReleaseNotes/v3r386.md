2019-08-13 AppConfig v3r386
========================================

This version is released on `master` branch.  

### Simulation  

- Option file to disable merging of GenFSR by Brunel, !88 (@kreps)  

