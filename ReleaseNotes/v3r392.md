

2019-11-14 AppConfig v3r392
========================================

Release for second validation of S24r2, S28r2, S29r2p1
----------------------------------------

This version is released on master branch.

- options changes for S24r2, S28r2, S29r2p1, !104 (@nskidmor)   
  
