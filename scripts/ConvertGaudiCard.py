#! /usr/bin/env python
'''Convert gaudi cards to new format or to old format
'''
from __future__ import print_function
Persistency=None
output=None
defaultOutput='new'
inputs=[]
setPersistency=False

import sys
 
# a simple class with a write method
class WritableObject:
    def __init__(self):
        self.content = []
    def write(self, string):
        self.content.append(string)

try:
    from GaudiConf import IOHelper, IOExtension
except ImportError:
    raise ImportError("IOHelper does not exist in this application version. Getpack a more recent GaudiConf or switch to a more recent application")

def usage(name):
    print('usage: '+name+' <inputfile> [<inputfile>...] [--new/--ext/--old] [--persistency=<POOL,ROOT,MDF>] [--setpersistency]')
    print('   inputfile: gaudi card or options file to parse')
    print('   --new: (default) output in new IOHelper style')
    print('   --ext: output in new IOExtension style')
    print('   --old: output in old GaudiCard style')
    print('   --persistency: Convert persistency to given format, otherwise use default')
    print('   --setpersistency: explicitly give the persistency in the card, even if it is the default')

def checkArgs(args,name):
    global output
    global inputs
    global Persistency
    global setPersistency
    if len(args)<1:
        usage(name)
        print("Error: you must supply an input file")
        sys.exit(1)
    for arg in args:
        if arg.lower() in ['help', '--help', '-h']:
            usage(name)
            sys.exit(0)
        if arg.lower() in ['--new', '--ext', '--old']:
            if output is None:
                output=arg.lower().replace('--','')
            elif output!=arg:
                print("Error: can't be both new and old format")
                sys.exit(1)
        elif '--persistency' in arg.lower():
            Persistency=arg.upper().split('=')[-1].strip('"').strip("'")
        elif '--setpersistency' in arg.lower():
                        setPersistency=True
        else:
            import os
            arg=os.path.expandvars(os.path.expanduser(arg))
            if os.path.isfile(arg):
                inputs.append(arg)
            else:
                print("Error: file "+arg+" does not exist")
                sys.exit(1)


#parse arguements
checkArgs(sys.argv[1:], sys.argv[0])

if output is None:
    output=defaultOutput

#redirect stdout and stderr

sys.stdout=WritableObject()

#exec all inputfiles, protected in a local scope
for input in inputs:
    execfile(input)
sys.stdout=sys.__stdout__

ioh=IOHelper(Persistency,Persistency)
iox=IOExtension(Persistency)

if Persistency is not None:
    ioh.convertSelector()
    setPersistency=True

if output=='new':
    print(ioh.helperString(setPersistency=setPersistency))
elif output=='ext':
    print(iox.extensionString(setPersistency=setPersistency))
else:
    print(ioh.selectorString())

